def group_courses():
    # Iterate through the weekdays for the course.
    for weekday in course["days"]:
        flag = False      
        # Iterate through the possible days.
        for day in each_group["days"]:
            # If a match is found, add the course to the groupings.
            if weekday == day:
                each_group["enrollment"] += course["enrollment"]
                each_group["classes"].append(course)

                # Set flag to break if match found.
                flag = True
                break

            if flag == True:
                break
