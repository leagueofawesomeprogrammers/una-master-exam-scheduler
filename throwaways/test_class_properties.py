class Test(object):
    def __init__(self):
        self.__x = 1
        self.__y = 1
        self.__d = {'one':1,'two':2}

    @property
    def get_x(self):
        return self.__x

    @property
    def get_y(self):
        return self.__y


    @get_x.setter
    def set_x(self, i):
        self.__x += i * 2
    
    @get_y.setter
    def set_y(self, i, j, k):
        self.__y = i + j + k
