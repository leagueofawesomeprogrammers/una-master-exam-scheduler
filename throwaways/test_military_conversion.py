#This is a test function that was designed to add two sets of minutes together
#and to possibly add the result to some current military time represented as
#an integer.
def test_military(one, two, current_time=None):
    hour = 0
    minute = 0
    hour = ( (one + two)/60 ) * 100
    minute = (one + two)%60
    if current_time:
            current_time_str = str(current_time)
            current_time_str = "0"*(4-len(current_time_str)) + current_time_str
            new_hour = ((int(current_time_str[2:]) + minute)/60) * 100
            hour = ((int(current_time_str[:2])*100) + hour + new_hour)
            minute = (int(current_time_str[2:]) + minute)%60
    return hour + minute
