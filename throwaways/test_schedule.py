from schedule import Schedule as sched

def check_classes_to_schedule(file_name, schedule):
    class_names = []
    with open(file_name, "r") as file_handle:
        class_names = file_handle.read().replace(" ", "").split("\n")
        
    valid = []
    invalid = []
    all_schedule_courses = []
    print class_names

    for day_num, each_day in enumerate(schedule):
        for each_exam in each_day:
            for each_course in each_exam["courses"]:
                    days = each_course["days"]
                    start_time = each_course["start_time"].strftime("%I%M")
                    end_time = each_course["end_time"].strftime("%I%M")
                    enrollment = str(each_course["enrollment"])
                    course_str = days + start_time + "-" + end_time + "," + enrollment

                    all_schedule_courses.append((course_str, day_num))

    for each_class in class_names:
        flag = False
        for course, day_num in all_schedule_courses:
            if each_class == course:
                data = (each_class, day_num)
                valid.append(data)
                flag = True
                break
        if not flag:
            invalid.append(each_class)

    print "-----------------<Courses found in Schedule>---------------------"
    for each_class in class_names:
        valid_list = [each[0] for each in valid]
        count = valid_list.count(each_class)
        if count > 0:
            day_nums = [str(each[1]) for each in valid if each_class == each[0]]
            print "Course: ", each_class, ";\tCount in Schedule:", count, ";\tDay:", ",".join(day_nums)
    print "-----------------</Courses found in Schedule>---------------------"

    print "-----------------<Courses NOT found in Schedule>---------------------"
    for each_class in invalid:
        print "Course:", each_class
    print "-----------------</Courses NOT found in Schedule>---------------------"

    print "Total valid:", len(valid)
    print "Total invalid:", len(invalid)
    print "Added valid and invalid:", len(valid) + len(invalid)
    print "Total classes:", len(class_names)

def check_schedule_to_classes(file_name, schedule):
    class_names = []
    with open(file_name, "r") as file_handle:
        class_names = file_handle.read().replace(" ", "").split("\n")
        
    valid = []
    invalid = []
    print class_names

    for day_num, each_day in enumerate(schedule):
        for each_exam in each_day:
            for each_course in each_exam["courses"]:
                    days = each_course["days"]
                    start_time = each_course["start_time"].strftime("%I%M")
                    end_time = each_course["end_time"].strftime("%I%M")
                    enrollment = str(each_course["enrollment"])
                    course_str = days + start_time + "-" + end_time + "," + enrollment
                    if course_str in class_names:
                        data = (course_str, day_num)
                        valid.append(data)
                    else:
                        invalid.append(course_str)

    print "-----------------<Courses from Schedule found in File>---------------------"
    for each_class in class_names:
        valid_list = [each[0] for each in valid]
        count = valid_list.count(each_class)
        if count > 0:
            day_nums = [str(each[1]) for each in valid if each_class == each[0]]
            print "Course: ", each_class, ";\tCount in Schedule:", count, ";\tDay:", ",".join(day_nums)
    print "-----------------</Courses from Schedule found in File>---------------------"

    print "-----------------<Courses from Schedule NOT found in File>---------------------"
    for each_class in invalid:
        print "Course:", each_class
    print "-----------------</Courses from Schedule NOT found in File>---------------------"

    print "Total valid:", len(valid)
    print "Total invalid:", len(invalid)
    print "Added valid and invalid:", len(valid) + len(invalid)
    print "Total classes:", len(class_names)

def main(file_name = None):
    #this script does not seem to run properly unless it is in the same directory
    #as the schedule
    if file_name is None:
        file_name = 'Fall 2014 Total Enrollments by Meeting times.csv'
    
    test_sched = sched()
    test_sched.load_courses_from_file(file_name)
    print 'courses loaded!'
    test_sched.load_parameters_from_file('parameters.csv')
    print 'parameters loaded!'
    test_sched.group_courses()
    print 'courses grouped!'
    test_sched.validate_schedule()
    print 'schedule validated!'
    result = test_sched.generate_schedule()
    print 'schedule generated!'

    if result is True:
        print 'SUCCESS!!!!!!!!'
        for i, each_day in enumerate(test_sched.schedule):
            print '--------------start day', i + 1, '---------------'
            for each_exam in each_day:
                print '\n'
                print each_exam

            print '-------------end day', i + 1, '----------------'

        check_classes_to_schedule(file_name, test_sched.schedule)
        test_sched.save_schedule_to_pdf("schedule.pdf")
        test_sched.save_schedule_to_text_file("schedule.txt")
        test_sched.save_schedule_to_csv("schedule.csv")

    else:
        print 'BROKEN!!!!!!'
        print 'Broken on day', day
        for i, each_day in enumerate(result):
            print '--------------start day', i + 1, '---------------'
            for each_exam in each_day:
                print '\n'
                print each_exam

            print '-------------end day', i + 1, '----------------'
            
        check_classes_to_schedule(file_name, result)
        test_sched.save_schedule_to_pdf("schedule.pdf")
        test_sched.save_schedule_to_text_file("schedule.txt")
        test_sched.save_schedule_to_csv("schedule.csv")

if __name__ == "__main__":
    main()
