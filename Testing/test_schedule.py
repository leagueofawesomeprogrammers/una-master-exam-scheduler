from schedule import Schedule as sched

#this script does not seem to run properly unless it is in the same directory
#as the schedule
test_sched = sched()
test_sched.load_courses_from_file('Fall 2014 Total Enrollments by Meeting times.csv')
print 'courses loaded!'
test_sched.load_parameters_from_file('parameters.csv')
print 'parameters loaded!'
test_sched.group_courses()
print 'courses grouped!'
test_sched.validate_schedule()
print 'schedule validated!'
result = test_sched.generate_schedule()
print 'schedule generated!'

if result == True:
    print 'SUCCESS!!!!!!!!'
    for i, each_day in enumerate(test_sched.schedule):
        print '--------------start day', i + 1, '---------------'
        for each_exam in each_day:
            print '\n'
            print each_exam

        print '-------------end day', i + 1, '----------------'

else:
    print 'BROKEN!!!!!!'
    for i, each_day in enumerate(result):
        print '--------------start day', i + 1, '---------------'
        for each_exam in each_day:
            print '\n'
            print each_exam

        print '-------------end day', i + 1, '----------------'

test_sched.save_schedule_to_pdf('test.pdf')
test_sched.save_schedule_to_csv('test.csv')
test_sched.save_schedule_to_text_file('test.txt')
