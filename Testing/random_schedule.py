import datetime
import random

def get_schedule():
    schedule = []

    #Loop for each day.
    for day in range(2):
        courses = []

        #Loop to generate the number of courses for a certain exam time.
        for i in range(10):
            course = {'days':'MWF', 'start_time':None, 'end_time': None, 'enrollment':None}
            start = datetime.datetime(1990,1,1,random.randint(7,17),0)
            end = start + datetime.timedelta(minutes=50)
            course['start_time'] = start
            course['end_time'] = end
            course['enrollment'] = random.randint(0,500)
            courses.append(course)

        exams = []

        #Loop to generate the number of exams for a given day.
        for i in range(5):
            exam = {'exam_start_time': None, 'days': 'MWF', 'courses': [], 'class_time': None}
            start = datetime.datetime(1990,1,1,random.randint(7,17),0)
            classtime = datetime.datetime(1990,1,1,random.randint(7, 17),0)
            exam['class_time'] = classtime
            exam['exam_start_time'] = start
            #Loop through the courses and append two courses to each exam time.
            for j in range(0, 10, 2):
                exam['courses'].append(courses[j])
                exam['courses'].append(courses[j+1])
            exams.append(exam)

        schedule.append(exams)

    return schedule

