Param1.csv
	- Invalid number of days:  0


Param2.csv 
	- NO ERRORS


Param3.csv
	- NO ERRORS


Param4.csv
	- Invalid number of days:  6


Param5.csv
	- Invalid number of days:  -1


Param6.csv
	- Invalid exam length:  74


Param7.csv
	- NO ERRORS


Param8.csv
	- Invalid intermission length:  5


Param9.csv
	- NO ERRORS


Param10.csv
	- NO ERRORS


Param11.csv
	- NO ERRORS


Param12.csv
	- Invalid intermission length:  31


Param13.csv
	- NO ERRORS


Param14.csv
	- Invalid exam length:  0


Param15.csv
	- NO ERRORS


Param16.csv
	- NO ERRORS


Param17.csv
	- Invalid intermission:  -10   (will display as 1430 -- counting backwards)


Param18.csv
	- Invalid exam length:  -30    (will display as 1410 -- counting backwards)


Param19.csv
	- Invalid exam time 0765       (generic error:  minutes between 0-59)


Param20.csv
	- Invalid format  (generic error 4 0765)


Param21.csv
	- Invalid exam start time 0600


Param22.csv
	- Invalid exam start time 0729


Param23.csv
	- NO ERRORS


Param24.csv
	- NO ERRORS


Param25.csv
	- Invalid lunch length       (will display as 1410 -- counting backwards)


	