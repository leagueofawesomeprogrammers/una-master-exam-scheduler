from distutils.core import setup
import py2exe

import os

for each in os.listdir('..\\dist'):
	os.remove('..\\dist\\'+ each)

setup(
    data_files=[
	"help.bmp",
        "poptimes_x.bmp",
        "una_icon.ico",
        "una_athletic_logo.png",
        "UserManual.pdf"
    ],
    windows=[
	{
            "script": "main.py",
            "uac_info": "requireAdministrator",
            "icon_resources": [(0, "una_icon.ico")]
        }
    ],
    console=[
        {
            "script": "main.py",
            "icon_resources": [(0, "una_icon.ico")]
        }
    ],
    options={
        "py2exe": {
            "dist_dir": "C:\\Users\\Casey\\Desktop\\una-master-exam-scheduler\\dist",
            "includes": [
                "re",
                "random",
                "wx",
                "pickle",
                "os",
                "time",
                "datetime"
            ],
            "packages": ["reportlab"],
            "bundle_files": 3,
            "optimize": 2
        }
    }
)
