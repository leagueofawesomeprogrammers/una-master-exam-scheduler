import schedule
import gui
import wx


class Driver():
    def __init__(self):
        self.the_schedule = schedule.Schedule()


    def main(self):
        """
        This method is the main function that will call all aspects of the GUI
        and methods from schedule.py
        
        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 4/30/2015
        Modified by: Jonathon Marlar 5/01/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # bind load_parameters, set_parameters, parameters variable
        interface = wx.App()
        main_window = gui.MainWindow(None,
            create_schedule = self.setup_schedule,
            load_parameters = self.load_parameters,
            set_parameters = self.the_schedule.set_parameters,
            save_csv = self.save_csv,
            save_pdf = self.save_pdf,
            save_txt = self.save_txt,
            save_umes = self.save_umes,
            load_umes = self.load_umes,
            get_file_a = self.get_file_a,
            get_file_b = self.get_file_b,
            get_start_times = self.get_start_times,
            reschedule = self.reschedule,
            get_night_classes = self.get_night_classes)
        interface.MainLoop()
        
    def load_parameters(self, filename):
        """
        This method sets up, validates, and creates the parameters.
        
        Parameters
        ----------
        filename - the path for the parameter file


        Return
        ------
        the_schedule.parameters


        Author(s): Justin Brumley
        Created: 5/2/2015
        Modified by:
        Files accessed: parameter file that is chosen by the user
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        try:
            self.the_schedule.load_parameters_from_file(filename)
        except Exception as error:
            raise error
        
        return self.the_schedule.parameters

    def setup_schedule(self, file_a, file_b):
        """
        This method sets up, validates, and creates the schedule by calling
        methods from schedule.py
        
        Parameters
        ----------
        file_a - the path for the parameter file
        file_b - the path for the class enrollment data file


        Return
        ------
        the_schedule.schedule


        Author(s): Jonathon Marlar
        Created: 4/30/2015
        Modified by: Jonathon Marlar 5/01/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        # exam parameters
        try:
            self.the_schedule = None
            self.the_schedule = schedule.Schedule()
            self.the_schedule.load_parameters_from_file(file_a)
            self.the_schedule.set_parameters(self.the_schedule.parameters)
            self.the_schedule.save_parameters_to_file(file_a)
    
            # course data
            self.the_schedule.load_courses_from_file(file_b)
            self.the_schedule.group_courses()
    
            # making schedule using params/course data
            self.the_schedule.get_schedule_times()
            self.the_schedule.validate_schedule()
    
            self.the_schedule.generate_schedule()
            
        except Exception as error:
            raise error
            
        return self.the_schedule.schedule

    def save_csv(self, file_name):
        """
        This method saves course data to the given filename as a comma separated
        value file
        
        Parameters
        ----------
        file_name - the full directory and file name to save to


        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 5/01/2015
        Modified by: Jonathon Marlar 5/01/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        self.the_schedule.save_schedule_to_csv(file_name)

    def save_pdf(self, file_name):
        """
        This method saves course data to the given filename as a portable
        document format file
        
        Parameters
        ----------
        file_name - the full directory and file name to save to


        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 5/01/2015
        Modified by: Jonathon Marlar 5/01/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        self.the_schedule.save_schedule_to_pdf(file_name)

    def save_txt(self, file_name):
        """
        This method saves course data to the given filename as a text file
        
        Parameters
        ----------
        file_name - the full directory and file name to save to


        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 5/01/2015
        Modified by: Jonathon Marlar 5/01/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        self.the_schedule.save_schedule_to_text_file(file_name)
        
    def save_umes(self, file_name, file_a_path, file_b_path):
        """
        This method saves course data to the given filename as a .umes file.
        
        Parameters
        ----------
        file_name - the full directory and file name to save to


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 5/3/2015
        Modified by:
        Files accessed: Filename chosen by user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.the_schedule.file_a = file_a_path
        self.the_schedule.file_b = file_b_path
        self.the_schedule.save_schedule_to_umes(file_name)
        
    
    def load_umes(self, file_name):
        """
        This method loads the schedule information from a .umes file.
        
        Parameters
        ----------
        file_name - the full directory and file name to load from


        Return
        ------
        schedule object returned from 
        self.the_schedule.load_schedule_from_file(file_name)


        Author(s): Justin Brumley
        Created: 5/3/2015
        Modified by:
        Files accessed: Filename chosen by user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.the_schedule = schedule.Schedule.load_schedule_from_umes(file_name)
        return self.the_schedule.schedule
    
    def get_file_a(self):
        """
        This method returns file_a from a schedule.
        
        Parameters
        ----------
        None

        Return
        ------
        self.the_schedule.file_a - string that contains the file path for 
                                   file_a


        Author(s): Justin Brumley
        Created: 5/3/2015
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        return self.the_schedule.file_a
        
    def get_file_b(self):
        """
        This method returns the file_b path from the schedule.
        
        Parameters
        ----------
        None

        Return
        ------
        self.the_schedule.file_a - string that contains the file path for 
                                   file_b


        Author(s): Justin Brumley
        Created: 5/3/2015
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        return self.the_schedule.file_b
    
    def get_start_times(self):
        """
        This method returns a list of start times.
        
        Parameters
        ----------
        None

        Return
        ------
        a list of start times


        Author(s): Justin Brumley
        Created: 5/3/2015
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        return self.the_schedule.get_schedule_times()
    
    def reschedule(self):
        """
        This method returns a different instance of a schedule.
        
        Parameters
        ----------
        None

        Return
        ------
        self.the_schedule.schedule - a new schedule


        Author(s): Justin Brumley
        Created: 5/4/2015
        Modified by:
        Files accessed: None 
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.the_schedule.generate_groups()
        self.the_schedule.get_schedule_times()
        self.the_schedule.validate_schedule()
        self.the_schedule.generate_schedule()

        return self.the_schedule.schedule  
    
    def get_night_classes(self):
        """
        This method returns the night classes from the schedule class.
        
        Parameters
        ----------
        None

        Return
        ------
        self.the_schedule.schedule - a new schedule


        Author(s): Justin Brumley
        Created: 5/4/2015
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        return self.the_schedule.night_classes
        

if __name__ == "__main__":
    dr = Driver()
    dr.main()
