import os
import re
import subprocess
import time
import wx
import wx.grid
import wx.lib.scrolledpanel
from datetime import datetime
from wx._core import Position, GetApp

class CreateScheduleWindow(wx.Frame):
    """
    The CreateScheduleWindow contains all the GUI components and functionality
    for making the popup that appears when the user hits the "Create Schedule"
    button.
    """

    def __init__(self, parent):
        """
        Initialization of the "Create Schedule" window and its functionality

        Parameters
        ----------
        parent - main window


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/22/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        wx.Frame.__init__(self, parent,
            title = "Create Schedule",
            style=wx.SYSTEM_MENU|wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.init_ui()
        
    def init_ui(self):
        """
        Sets up the GUI for the "Create Schedule" popup

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 5/02/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        # Set the window properties.
        self.window_size = (450, 700)
        self.MakeModal();

        # We setup the sizers for the create schedule window.
        outer_grid = wx.BoxSizer(wx.VERTICAL)
        inner_grid = wx.FlexGridSizer(7, 2, 10, 10)
        
        # We define the font for the window to use throughout use.
        font = wx.Font(12, wx.ROMAN, wx.NORMAL, wx.NORMAL, False,
            u"Times New Roman")
        self.SetFont(font)

        # The list of parameters for the first file are initialized.
        self.file_a_data = [""]*5
        
        self.panel = wx.Panel(self)
        self.panel.SetSizer(outer_grid)
        
        bmp = wx.Image("help.bmp", wx.BITMAP_TYPE_BMP).ConvertToBitmap()
        bmp.SetMaskColour((0, 255, 255))
        self.btn_help = wx.StaticBitmap(self.panel, -1, bmp, 
            (10, self.window_size[1] - 50), (bmp.GetWidth(), bmp.GetHeight()))
        
        # We set up the label and layout of the GUI that accepts File A.
        lbl_file_a_input = wx.StaticText(self.panel,
            label="Session Times Information")
        
        # Setup font formatting for the "head" label.
        bold_font = lbl_file_a_input.GetFont()
        bold_font.SetWeight(wx.BOLD)
        bold_font.SetStyle(wx.ITALIC)
        lbl_file_a_input.SetFont(bold_font)
        
        # We then set up the labels for the text boxes.
        lbl_number_of_days = wx.StaticText(self.panel,
            label="Number of days to schedule: ")
        lbl_begin_time = wx.StaticText(self.panel,
            label="Begin time for the first exam of the day (24 hour): ")
        lbl_time_for_exam = wx.StaticText(self.panel,
            label="Length of time for each exam (in minutes): ")
        lbl_time_between_exam = wx.StaticText(self.panel,
            label="Length of time between exams (in minutes): ")
        lbl_lunch_period = wx.StaticText(self.panel,
            label="Length of time for a lunch period (in minutes): ")
        
        btn_load_from_file = wx.Button(self.panel,
            label="Fill out with a file")
        
        # Text boxes for the create schedule window.
        self.txt_number_of_days = wx.TextCtrl(self.panel)
        self.txt_begin_time = wx.TextCtrl(self.panel)
        self.txt_time_for_exam = wx.TextCtrl(self.panel)
        self.txt_time_between_exams = wx.TextCtrl(self.panel)
        self.txt_lunch_period = wx.TextCtrl(self.panel)

        # This list is for checking if each of the boxes were filled.
        self.csw_textctrl_list = [self.txt_number_of_days, self.txt_begin_time,
            self.txt_time_for_exam, self.txt_time_between_exams,
            self.txt_lunch_period]

        btn_load_from_file.Bind(wx.EVT_BUTTON, self.on_load_file_a)
        self.btn_help.Bind(wx.EVT_LEFT_DOWN, self.on_help_button)
        
        inner_grid.AddMany([lbl_file_a_input,
            (btn_load_from_file, 0, wx.ALIGN_RIGHT),
            lbl_number_of_days,
            (self.txt_number_of_days, 0, wx.ALIGN_RIGHT), 
            lbl_begin_time,
            (self.txt_begin_time, 0, wx.ALIGN_RIGHT),
            lbl_time_for_exam,
            (self.txt_time_for_exam, 0, wx.ALIGN_RIGHT),
            lbl_time_between_exam,
            (self.txt_time_between_exams, 0, wx.ALIGN_RIGHT),
            lbl_lunch_period,
            (self.txt_lunch_period, 0, wx.ALIGN_RIGHT)])
        
        outer_grid.Add(inner_grid, proportion=5, flag=wx.ALL|wx.EXPAND,
            border=15)
        
        # We create the label and button for the enrollment data.
        lbl_session_data = wx.StaticText(self.panel,
            label="Session Data (.csv)")
        lbl_session_data.SetFont(bold_font)

        btn_session_data = wx.Button(self.panel,
            label="Load Data From File")
        btn_session_data.Bind(wx.EVT_BUTTON, self.on_load_file_b)

        inner_grid.AddMany([lbl_session_data, (btn_session_data, 0, 
            wx.ALIGN_RIGHT)])
        
        self.lbl_file_b_name = wx.StaticText(self.panel, label="")
        
        file_name = wx.GetApp().TopWindow.file_b_path
        if file_name == "":
            self.lbl_file_b_name = wx.StaticText(self.panel,
                label="File Not Chosen")
        else:
            self.lbl_file_b_name.SetLabel(file_name)
        
        outer_grid.Add(self.lbl_file_b_name, proportion=5,
            flag=wx.ALL|wx.EXPAND, border=15)

        # We then create the bottom buttons for making a schedule.
        bottom_grid = wx.BoxSizer(wx.HORIZONTAL)
        btn_csw_create = wx.Button(self.panel,
            label="Create Schedule")
        btn_csw_cancel = wx.Button(self.panel,
            label="Cancel")
        btn_csw_create.Bind(wx.EVT_BUTTON, self.on_csw_create)
        btn_csw_cancel.Bind(wx.EVT_BUTTON, self.on_quit)
        
        outer_grid.Add(bottom_grid, flag=wx.ALIGN_RIGHT|wx.BOTTOM|wx.RIGHT,
            border=10)
        bottom_grid.AddMany([btn_csw_cancel, btn_csw_create])
        
        self.Bind(wx.EVT_CLOSE, self.on_quit)
        
        self.SetSize((450, 470))
        self.CenterOnParent();

    def on_csw_create(self, event):
        """
        Destroy the window and call functions to create schedule

        Parameters
        ----------
        event - A button click


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/21/15
        Modified by: Jonathon Marlar 5/02/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        # Give an error if the text boxes are empty or no enrollment file.
        if (any(txt.IsEmpty() for txt in self.csw_textctrl_list) or
        not self.GetParent().file_b_path):
            wx.MessageBox("Error: Data Not Inserted", "Error",
                wx.OK | wx.ICON_ERROR)
        else:
            # Pass back files to be used.
            new_file_a = []
            make_new_file = False

            # Check if the user has edited the parameters.
            for i, text in enumerate(self.csw_textctrl_list):
                new_file_a.append(text.GetValue())
                if text.GetValue() != self.file_a_data[i]:
                    make_new_file = True

            # If they edited the parameters, create a new file.
            if make_new_file:
                filename = "parameters" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + \
                    ".csv"
                with open(filename, "w") as file_handle:
                    file_handle.write(",".join(new_file_a))
                self.GetParent().file_a_path = filename

            # Supply the file a and file b paths to be passed to the maker.
            if self.GetParent().bindings["create_schedule"] is not None:
                filea = self.GetParent().file_a_path
                fileb = self.GetParent().file_b_path
                try:
                    p = self.GetParent()
                    p.sched = p.bindings["create_schedule"](filea, fileb)
                    p.toggle_buttons()
                    p.build_schedule_ui()
                    self.on_quit(event)
                except Exception as error:
                    wx.MessageBox(str(error), "Error",
                wx.OK | wx.ICON_ERROR)
        
    def on_help_button(self, event):
        """
        Destroy the window and call functions to create schedule

        Parameters
        ----------
        event - A button click


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/22/15
        Modified by:
        Files accessed: !!!!!(help.pdf)!!!!!
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        self.GetParent().on_user_manual(event)

    def on_load_file_a(self, event):
        """
        Opens a file dialog to find File A to be used in the
        CreateScheduleWindow

        Parameters
        ----------
        event - A button click


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/21/15
        Modified by: Jonathon Marlar 5/02/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        parent = self.GetParent()
        
        # Put the data from the parameters file into a list for checking.
        explorer = wx.FileDialog(self, "Open CSV file", "", "",
            "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if explorer.ShowModal() == wx.ID_OK:
            f = explorer.GetFilename()
            d = explorer.GetDirectory()
            with open(os.path.join(d, f), "r") as openf:
                file_content = openf.read()
                file_parameters = file_content.split(",")

            try:
                # Load the parameters to the schedule object.
                if parent.bindings["load_parameters"] is not None:
                    self.parameters = parent.bindings["load_parameters"](os.path.join(d, f))
                    self.txt_number_of_days.SetValue(str(self.parameters["exam_day_amount"]))
                    self.txt_begin_time.SetValue(self.parameters["begin_time"].strftime("%H%M"))
                    time_for_exam = str(self.parameters["exam_time"].seconds/60)
                    self.txt_time_for_exam.SetValue(time_for_exam)
                    time_between_exams = str(self.parameters["intermission_time"].seconds/60)
                    self.txt_time_between_exams.SetValue(time_between_exams)
                    time_for_lunch = str(self.parameters["lunch_time"].seconds/60)
                    self.txt_lunch_period.SetValue(time_for_lunch)
                for i, text in enumerate(self.csw_textctrl_list):
                    text.SetValue(file_parameters[i])
                    self.file_a_data[i] = file_parameters[i]
            except Exception as error:
                wx.MessageBox(str(error), "Error",
                    wx.OK | wx.ICON_ERROR)
            
            self.GetParent().set_file_a_path(os.path.join(d,f)) 

    def on_load_file_b(self, event):
        """
        Opens a file dialog to find File B to be used in the
        CreateScheduleWindow

        Parameters
        ----------
        event - A button click


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/21/15
        Modified by: Jonathon Marlar 3/22/15
                             Justin Brumley 3/22/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        explorer = wx.FileDialog(self, "Open text file", "", "",
            "CSV files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if explorer.ShowModal() == wx.ID_OK:
            # Set the file B path.
            f = explorer.GetFilename()
            d = explorer.GetDirectory()
            self.lbl_file_b_name.SetLabel(f)
            self.GetParent().set_file_b_path(os.path.join(d, f)) 
            
    def on_quit(self, event):
        """
        Called when the window is closed and handles cleanup

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
                     Justin Brumley 3/21/15
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.MakeModal(False)
        self.Destroy()


class MainWindow(wx.Frame):
    """
    The MainWindow class will create the overall layout of the "main window",
    the window that contains everything to access GUI functionality
    """
    
    def __init__(self, *args, **kwargs):
        """
        Initialization of the main window and its functionality

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        expected_bindings = [
            "create_schedule",
            "save_csv",
            "save_pdf",
            "save_txt",
            "save_umes",
            "load_parameters",
            "set_parameters",
            "get_file_a",
            "get_file_b",
            "load_umes",
            "get_start_times",
            "reschedule",
            "get_night_classes"
            ]
        
        
        self.bindings = {}
        
        for each_expected in expected_bindings:
            if kwargs[each_expected] is not None:
                self.bindings[each_expected] = kwargs[each_expected]
                del kwargs[each_expected]
        
        super(MainWindow, self).__init__(*args, **kwargs)
        self.init_ui()

        self.icon = wx.Icon("una_icon.ico", wx.BITMAP_TYPE_ICO)
        self.SetIcon(self.icon)
          
    def init_ui(self):
        """
        Sets up the GUI for the main window

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Justin Brumley 4/2/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Initialize variables that will be used in setting file paths/save data 
        self.has_been_saved = False
        self.has_been_modified = True
        self.file_path = ""
        self.file_a_path = ""
        self.file_b_path = ""
        
        # Set up the menus and panels.
        self.menu_bar = wx.MenuBar()
        #self.edit_menu = wx.Menu()
        self.view_menu = wx.Menu()
        self.help_menu = wx.Menu()
        self.sched = None
        self.schedule = None
        self.night_classes = None
        
        # Set the font for the software.
        font = wx.Font(12, wx.ROMAN, wx.NORMAL, wx.NORMAL, False,
            u"Times New Roman")
        self.SetFont(font)

        # Setting up the splitter for the Show Popular Times split view
        self.splitter = wx.SplitterWindow(self)
        self.pop_times_panel = wx.Panel(self.splitter, style=wx.BORDER_SUNKEN)
        self.pop_times_panel.Show(False)
        
        # This is where we setup the overall sizer and toolbar.
        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.AddSpacer(10)
        self.panel = wx.Panel(self.splitter)
        self.splitter.Initialize(self.panel)
        self.panel.SetFont(font)
        self.panel.SetSizer(self.main_sizer)
        self.main_sizer.Add(button_sizer, flag=wx.ALL|wx.EXPAND, border=5)
        
        
        # Here, we create the interface buttons.
        self.btn_create_schedule = wx.Button(self.panel,
            label = "Create Schedule")
        self.btn_open_schedule = wx.Button(self.panel,
            label = "Open Schedule")
        self.btn_save_schedule = wx.Button(self.panel,
            label = "Save Schedule")
        self.btn_duplicate_schedule = wx.Button(self.panel,
            label = "Duplicate Schedule")
        self.btn_reschedule = wx.Button(self.panel,
            label = "Reschedule")
        
        self.btn_reschedule.Hide()
        self.btn_duplicate_schedule.Hide()
        self.btn_save_schedule.Hide()
        
        # The buttons are added to the main_sizer sizer which is a child 
        # to self.panel
        button_sizer.Add(self.btn_create_schedule, flag=wx.ALL, border=5)
        button_sizer.Add(self.btn_open_schedule, flag=wx.ALL, border=5)
        button_sizer.Add(self.btn_save_schedule, flag=wx.ALL, border=5)
        button_sizer.Add(self.btn_duplicate_schedule, flag=wx.ALL, border=5)
        button_sizer.Add(self.btn_reschedule, flag=wx.ALL, border=5)
        
        #Adding a static line to separate controls
        self.main_sizer.Add(wx.StaticLine(self.panel),
            flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        # Now, we bind the buttons to actions.
        self.btn_create_schedule.Bind(wx.EVT_BUTTON, self.on_create_schedule)
        self.btn_open_schedule.Bind(wx.EVT_BUTTON, self.on_open_schedule)
        self.btn_save_schedule.Bind(wx.EVT_BUTTON, self.on_save_project)
        self.btn_duplicate_schedule.Bind(wx.EVT_BUTTON, self.on_export)
        self.btn_reschedule.Bind(wx.EVT_BUTTON, self.on_reschedule)
    
        # Edit menu items
        #self.item_user_preferences = self.edit_menu.Append(wx.ID_ANY,
        #    "User Preferences", "Manage user preferences")
        #self.item_manage_users = self.edit_menu.Append(wx.ID_ANY,
        #    "Manage Users", "Manage users")
        
        # View menu items
        self.item_show_popular_times = self.view_menu.Append(wx.ID_ANY,
            "Show Popular Times", "Show popular times")
        self.item_show_data_file_paths = self.view_menu.Append(wx.ID_ANY,
            "Show Data File Paths", "Show data file paths")
        self.item_view_day_mode = self.view_menu.Append(wx.ID_ANY,
            "Single Day Schedule", "View single day schedule")
        
        # Help menu items
        self.aboutitem = self.help_menu.Append(wx.ID_ANY,
            "About", "About")
        self.usermanualitem = self.help_menu.Append(wx.ID_ANY,
            "User Manual", "Display user manual")
        
        # We then disable menu items until they are needed.
        self.item_show_popular_times.Enable(False)
        self.item_show_data_file_paths.Enable(False)
        self.item_view_day_mode.Enable(False)
        
        
        #self.menu_bar.Append(self.edit_menu, "&Edit")
        self.menu_bar.Append(self.view_menu, "&View")
        self.menu_bar.Append(self.help_menu, "&Help")
        
        self.SetMenuBar(self.menu_bar)     
        
        self.Bind(wx.EVT_CLOSE, self.on_quit)
        
        # Edit menu action binding
        #self.Bind(wx.EVT_MENU,
        #    self.on_user_preferences, self.item_user_preferences)
        #self.Bind(wx.EVT_MENU,
        #    self.on_manage_users, self.item_manage_users)
        
        # View menu action binding
        self.Bind(wx.EVT_MENU,
            self.on_show_popular_times, self.item_show_popular_times)
        self.Bind(wx.EVT_MENU,
            self.on_show_data_file_paths, self.item_show_data_file_paths)
        self.Bind(wx.EVT_MENU,
            self.on_toggle_view_mode, self.item_view_day_mode)
        self.Bind(wx.EVT_SIZING, self.on_resize)
        self.Bind(wx.EVT_PAINT, self.on_resize)
        self.Bind(wx.EVT_MAXIMIZE, self.on_resize)
        
        # Help menu action binding
        self.Bind(wx.EVT_MENU, self.on_about, self.aboutitem)
        self.Bind(wx.EVT_MENU, self.on_user_manual, self.usermanualitem)

        # Popular times size
        self.pop_times_size = 0

        # Setup the pop_times_panel information
        self.gpt_sizer = wx.BoxSizer(wx.VERTICAL)
        self.pop_times_panel.SetSizer(self.gpt_sizer)
        self.grid_pop_times = wx.grid.Grid(self.pop_times_panel)
        self.grid_pop_times.CreateGrid(0,0)
        self.grid_pop_times.EnableEditing(False)
        self.grid_pop_times.SetRowLabelSize(30)
        self.grid_pop_times.SetColLabelValue(0, "Class")
        self.grid_pop_times.SetColLabelValue(1, "Enrolled")
        self.grid_pop_times.AppendCols(numCols=2)
        bmp = wx.Image("poptimes_x.bmp", wx.BITMAP_TYPE_BMP).ConvertToBitmap()
        bmp.SetMaskColour((0, 255, 255))
        self.btn_close_pop_times = wx.StaticBitmap(self.pop_times_panel,
            -1, bmp, (bmp.GetWidth(), bmp.GetHeight()))
        self.btn_close_pop_times.Bind(wx.EVT_LEFT_DOWN,
            self.on_show_popular_times)
        self.gpt_title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.gpt_title_sizer.Add(self.btn_close_pop_times)
        lbl_popular_times = wx.StaticText(self.pop_times_panel, 
            label="Popular Times")
        fnt = lbl_popular_times.GetFont()
        fnt.SetWeight(wx.BOLD)
        lbl_popular_times.SetFont(fnt)
        self.gpt_title_sizer.Add(lbl_popular_times, flag=wx.LEFT, border=30)
        self.gpt_sizer.Add(self.gpt_title_sizer)
        self.gpt_sizer.Add(self.grid_pop_times, flag=wx.ALL|wx.EXPAND)
    
        # Set the window properties and show it.
        self.SetTitle("UNA Master Exam Scheduler")
        self.CenterOnScreen()
        self.Show(True)
        self.Maximize()

    def build_schedule_ui(self):
        """
        This method builds the schedule interface after the schedule is
        created.

        Parameters
        ----------
        filename - a string that contains the file name
                   to assign to file_a_path


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/26/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.has_been_saved = False
        self.has_been_modified = True
        # First, get rid of any previous schedules made.
        if self.schedule is not None:
            self.schedule.Destroy()
        if self.night_classes is not None:
            self.night_classes.Destroy()

        # Create an instance of the ScheduleUI object and add it to the
        # main sizer.
        self.schedule = ScheduleUI(self.panel, (200, 200))
        self.main_sizer.Add(self.schedule, flag=wx.EXPAND, proportion=2)

        # Fill the popular times list set the parameters to the schedule object
        self.fill_popular_times_list()
        self.toggle_buttons()
        self.main_sizer.Layout()
        self.parameters = self.bindings["load_parameters"](self.file_a_path)

        # Show the night classes and lunch in the panel below the ScheduleUI.
        self.night_classes = NightClassUI(self.panel, (800, 100))
        self.main_sizer.Add(self.night_classes, flag=wx.EXPAND|wx.LEFT|wx.RIGHT,
            border=12, proportion=1)
        self.Layout()
        self.main_sizer.Layout()

    def fill_popular_times_list(self):
        """
        Populates side panel that will display the popular times

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 5/02/15
        Modified by: Jonathon Marlar 5/02/15
        Files accessed:
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        popular_times_list = []

        # Delete the grid before making new popular times.
        if self.pop_times_size != 0:
            self.grid_pop_times.DeleteRows(0, self.pop_times_size)
            
        # Populate the popular times list.
        for day in self.sched:
            for exam in day:
                for course in exam["courses"]:
                    popular_times_list.append(course)

        self.pop_times_size = len(popular_times_list)
                   
        # Sort them by most enrolled to least enrolled. 
        times = sorted(popular_times_list,
            key=lambda each: each["enrollment"], reverse=True)
        
        # Add the list to the Popular Times grid.
        for i, times in enumerate(times):
            self.grid_pop_times.AppendRows()
            self.grid_pop_times.SetCellValue(i, 0, str(times["days"] + times["start_time"].strftime(" %I:%M %p")))
            self.grid_pop_times.SetCellValue(i, 1, str(times["enrollment"]))
            self.grid_pop_times.SetColSize(0, 120)
            
        self.gpt_sizer.Layout()

    def on_about(self, event):
        """
        Opens a dialog box that displays information about the developers

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 4/18/15
                     Justin Brumley 3/21/15
        Files accessed:
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        description = """Developed by the League of Awesome Programmers:

        Kyle Kurzhal
        Jonathon Marlar
        Morgan Burcham
        Amberly Smith
        Justin Brumley
        """

        about = wx.AboutDialogInfo()
        about.SetIcon(wx.Icon("una_athletic_logo.png", wx.BITMAP_TYPE_PNG))
        about.SetName("UNA Master Exam Scheduler")
        about.SetDescription(description)
        about.SetVersion("1.0")
        about.SetCopyright("(C) 2015 University of North Alabama")

        wx.AboutBox(about)
        
    def on_create_schedule(self, event):
        """
        Creates the Create Schedule window

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        create_schedule_window = CreateScheduleWindow(self)
        create_schedule_window.Show(True)

    def on_export(self, event):
        """
        Opens a window to allow the user to save to a location on the disk

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
                     Justin Brumley 3/21/15
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        wildcard = "UNA Master Exam Scheduler file (*.umes)|*.umes" + \
            "|PDF file (*.pdf)|*.pdf|Comma Separated Value file " +\
            "(*.csv)|*.csv|Text file (*.txt)|*.txt"
        
        explorer = wx.FileDialog(self, "Save Project As", "", "", 
            wildcard, wx.FD_SAVE)
        
        if explorer.ShowModal() == wx.ID_OK:
            f = explorer.GetPath()
            if re.search(r"\.umes", f) != None:
                # Then we save the file as a .umes file
                # Write what data we need to write into the file
                self.has_been_modified = False
                self.has_been_saved = True
                self.file_path = f
                self.bindings["save_umes"](f, self.file_a_path, self.file_b_path)
            elif re.search(r"\.pdf", f) != None:
                if self.bindings["save_pdf"] is not None:
                    self.bindings["save_pdf"](f)
            elif re.search(r"\.csv", f) != None:
                if self.bindings["save_csv"] is not None:
                    self.bindings["save_csv"](f)
            elif re.search(r"\.txt", f):
                if self.bindings["save_txt"] is not None:
                    self.bindings["save_txt"](f)
            else:
                wx.MessageBox("Error", "Please choose a valid file type",
                    wx.OK)

    def on_manage_users(self, event):
        """
        Opens a window that will allow the administrator to edit users

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        pass
    
    def on_open_schedule(self, event):
        """
        Opens a window to allow the user to browse to a .uems file

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
                     Justin Brumley 3/21/15
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        explorer = wx.FileDialog(self, "Open Project", "", "", 
            "UNA Master Exam Scheduler file (*.umes)|*.umes", wx.FD_OPEN)
        
        if explorer.ShowModal() == wx.ID_OK:
            f = explorer.GetPath()
            self.sched = self.bindings["load_umes"](f)
            self.file_path = f
            self.file_a_path = self.bindings["get_file_a"]()
            self.file_b_path = self.bindings["get_file_b"]()
            self.build_schedule_ui()
            self.has_been_modified = False
            self.has_been_saved = True
    
    def on_quit(self, event):
        """
        Called when the program exits and handles cleanup

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if self.has_been_modified or not self.has_been_saved:
            warning = wx.MessageDialog(self, "Are you sure you want to" + \
                " quit without saving? \nAny unsaved changes will be lost.",
                "Confirm Exit", wx.OK | wx.CANCEL | wx.ICON_QUESTION)
            result = warning.ShowModal()
            if result == wx.ID_OK:
                self.Destroy()
        else:
            self.Destroy()

    def on_reschedule(self, event):
        """
        Triggers the scheduling process with different grouping

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
                     Justin Brumley 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if self.bindings["create_schedule"] is not None:
            self.sched = None
            self.sched = self.bindings["create_schedule"](self.file_a_path, self.file_b_path)
            self.build_schedule_ui()

    def on_resize(self, event):
        """
        This method takes care of any tasks that need to be addressed when
        the window is resized.

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        <return-name> - <return-description>


        Author(s): Justin Brumley
        Created: 3/28/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if self.splitter.IsShown():
            self.splitter.SetSashPosition(-230)
    
    def on_save_project(self, event):
        """
        Saves the project to the disk.

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/24/15
                     Justin Brumley 3/21/15
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # If the schedule hasn't been saved.
        if not self.has_been_saved:
            
            # This tells wxPython what file types to look for.
            wildcard = "UNA Master Exam Scheduler file (*.umes)|*.umes"
            
            explorer = wx.FileDialog(self, "Save Project As", "", "", 
                wildcard, wx.FD_SAVE)
            
            if explorer.ShowModal() == wx.ID_OK:
                f = explorer.GetPath()
                if re.search(r"\.umes", f) != None:
                    self.file_path = f
                    #Then we save the file as a .umes file.
                    self.has_been_modified = False
                    self.has_been_saved = True
                    
                    # Save the file through the back end.
                    self.bindings["save_umes"](f, self.file_a_path, self.file_b_path)
                else:
                    wx.MessageBox("Error", "Please choose a valid file type",
                        wx.OK)
        else:
            # Save the file through the back end.
            self.bindings["save_umes"](self.file_path, self.file_a_path, self.file_b_path)
            self.has_been_modified = False
            self.has_been_saved = True

    def on_show_data_file_paths(self, event):
        """
        Opens a dialog box that displays the file paths for the input files

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
                     Justin Brumley 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        wx.MessageBox("File A Path: " + self.file_a_path + \
            "\n \nFile B Path: " + self.file_b_path, "Data File Paths",
            wx.OK)

    def on_show_popular_times(self, event):
        """
        Opens a side panel that will display the popular times

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Justin Brumley 4/23/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if not self.pop_times_panel.IsShown():
            self.splitter.SplitVertically(self.panel,
                self.pop_times_panel, -250)
            self.splitter.SetMinimumPaneSize(70)
            self.item_show_popular_times.SetText("Hide Popular Times")
                
            self.gpt_sizer.Layout()
        else:
            self.splitter.Unsplit()
            self.item_show_popular_times.SetText("Show Popular Times")
            
    def on_toggle_view_mode(self, event):
        """
        Changes the generated schedule to a multiple day view or a single
        day view mode.

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if self.schedule.single_day_mode:
            self.schedule.view_multiple_day_schedule()
            self.panel.Layout()
            self.item_view_day_mode.SetItemLabel("Single Day Schedule")
        else:
            self.schedule.view_single_day_schedule()
            self.panel.Layout()
            self.item_view_day_mode.SetItemLabel("Multiple Day Schedule")
    
    def on_user_preferences(self, event):
        """
        Opens a window that will allow the user to edit their user preferences

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/21/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        pass
    
    def on_user_manual(self, event):
        """
        Opens the user manual in PDF format.

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 4/14/15
        Files accessed:UserManual.pdf
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        subprocess.call(("start", "UserManual.pdf"), shell=True)
    
    def set_file_a_path(self, filename):
        """
        Sets the path of file a to the filename string.

        Parameters
        ----------
        filename - a string that contains the file name
                   to assign to file_a_path


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/22/15
        Modified by:
        Files accessed: Depends on file specified.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.file_a_path = filename
        
    def set_file_b_path(self, filename):
        """
        Sets the path of file b to the filename string.

        Parameters
        ----------
        filename - a string that contains the file name
                   to assign to file_a_path


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/22/15
        Modified by:
        Files accessed: Depends on file specified.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.file_b_path = filename

    def toggle_buttons(self):
        """
        Toggles the visibility of the reschedule button.

        Parameters
        ----------
        event - required for all call back functions and carries
                information that wxPython requires


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Jonathon Marlar 3/23/15
                     Justin Brumley 3/23/15
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        self.btn_reschedule.Show()
        self.btn_duplicate_schedule.Show()
        self.btn_save_schedule.Show()
        self.panel.GetSizer().Layout()
        self.item_show_popular_times.Enable(True)
        self.item_show_data_file_paths.Enable(True)
        self.item_show_data_file_paths.Enable(True)
        self.item_view_day_mode.Enable(True)


class NightClassUI(wx.lib.scrolledpanel.ScrolledPanel):
    """
    This class is used to display the night class and lunch time
    information to the user interface.
    """
    
    def __init__(self, parent, dim=(1, 1)):
        """
        This method is the constructor for the NightUI class.
        
        Parameters
        ----------
        btn - the button to be recolored
                        
        Return
        ------


        Author(s): Justin Brumley
        Created: 4/16/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        super(NightClassUI, self).__init__(parent, -1, size=dim)
        self.init_ui()
        
    def init_ui(self):
        """
        Sets up the GUI for the night classes view

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Justin Brumley and Jonathon Marlar
        Created: 3/19/15
        Modified by: Justin Brumley 4/2/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        top_parent = self.GetTopLevelParent()
        
        self.outer_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.day_time_sizer = wx.BoxSizer(wx.VERTICAL)
        self.lunch_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Getting list of night classes and the lunch time.
        self.night_classes = top_parent.bindings["get_night_classes"]()
        lunch = top_parent.bindings["get_start_times"]()
        self.lunch_time = lunch[1]
        
        night_classes = wx.StaticText(self, label="Night Classes:")
        font = night_classes.GetFont()
        font.SetWeight(wx.BOLD)
        night_classes.SetFont(font)
        
        self.end_lunch_time = self.lunch_time + \
            top_parent.parameters["lunch_time"]
        self.day_time_sizer.Add(night_classes,
            flag=wx.BOTTOM, border=10)
        
        # Adding the night class labels to the sizer.
        for time in self.night_classes:
            lbl = wx.StaticText(self, 
                label=str(time["start_time"].strftime("%I:%M %p")) + " - " + \
                str(time["exam_end_time"].strftime(" %I:%M %p")), 
                    style=wx.BOLD)
            
            font = lbl.GetFont()
            font.SetWeight(wx.BOLD)
            lbl.SetFont(font)
            
            lbl2 = wx.StaticText(self, label="    " + str(time["days"]) + \
                " " + str(time["start_time"].strftime("%I:%M %p")) + " - " + \
                str(time["end_time"].strftime("%I:%M %p")))
            
            self.day_time_sizer.Add(lbl, flag=wx.EXPAND)
            self.day_time_sizer.Add(lbl2, flag=wx.EXPAND|wx.BOTTOM, 
                border=10)
        
        # Displaying the lunch time.
        lunch_lbl = wx.StaticText(self, label="Lunch Time:")
        font = lunch_lbl.GetFont()
        font.SetWeight(wx.BOLD)
        lunch_lbl.SetFont(font)
        
        self.lunch_sizer.Add(lunch_lbl, flag=wx.BOTTOM|wx.LEFT, border=15)
        
        self.lunch_sizer.Add(wx.StaticText(self, 
            label="    " + self.lunch_time.strftime("%I:%M %p") + \
            " - " + self.end_lunch_time.strftime("%I:%M %p")), 
            flag=wx.LEFT|wx.BOTTOM, border=15)
            
        self.outer_sizer.Add(self.day_time_sizer, flag=wx.EXPAND)
        self.outer_sizer.Add(self.lunch_sizer, flag=wx.EXPAND)
        
        self.SetSizer(self.outer_sizer)
        self.SetupScrolling()
        self.outer_sizer.Layout()
        self.Layout()


class ScheduleUI(wx.lib.scrolledpanel.ScrolledPanel):
    """
    This class handles the creation of the schedule User Interface.
    """
        
    def __init__(self, parent, dim=(1, 1)):

        """
        This method is the constructor for the ScheduleUI class.

        Parameters
        ----------
        parent - This parameter is the parent widget to the instance of the
                 class.
        
        dim - a tuple that holds dimension information for the table in which
              the schedule will reside


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/26/2015
        Modified by:
        Files accessed: File name is chosen by the user.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        super(ScheduleUI, self).__init__(parent, -1, size=(900, 600))
        self.init_ui(dim)
        self.btn_positions = {}

    def add_single_day_time_labels(self):
        """
        This method adds the time labels to the user interface.
        
        Parameters
        ----------
        None

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/9/15
        Modified by: 
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        self.time_label_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Add labels for the buttons in single day view
        for lbl in self.lbl_single_day_times:
            lbl.Bind(wx.EVT_LEFT_DOWN, self.on_mouse_down)
            lbl.Bind(wx.EVT_MOTION, self.on_mouse_move)
            self.time_label_sizer.Add(lbl, 
                flag=wx.LEFT|wx.BOTTOM,
                border=82)
            lbl.Raise()
            
        self.single_day_sizer.Add(self.time_label_sizer)

    def build_schedule(self, single):
        """
        This method builds the appropriate schedule depending on the current
        view mode.
        
        Parameters
        ----------
        single - a boolean value to indicate which kind of schedule buttons
                 the application should build

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/7/15
        Modified by: 4/21/2015 Cole Marlar, Justin Brumley
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        for i in range(len(self.schedule)):
            for j, sch in enumerate(self.schedule[i]):
                # Set the button's label and grouped class lists.
                if single:
                    if sch["enrollment"] != 0:
                        button = wx.Button(self, label=sch["days"] + " " + \
                            sch["class_time"], style=wx.NO_BORDER, 
                            size=self.single_button_size)
                        button.Show(False)
                        button.row = j
                        button.col = i
                    else:
                        button = wx.Button(self, label="",
                            style=wx.NO_BORDER, size=self.single_button_size)
                else:
                    if sch["enrollment"] != 0:
                        button = wx.Button(self, label=sch["days"] + " " + \
                            sch["class_time"], style=wx.NO_BORDER, 
                            size=self.multi_button_size)
                        button.row = j
                        button.col = i
                    else:
                        button = wx.Button(self, label="",
                            style=wx.NO_BORDER, size=self.multi_button_size)
                        button.row = j
                        button.col = i

                if button.GetLabel() != "":
                    if j % 2 == 0:
                        button.SetForegroundColour(self.color_p)
                        button.SetBackgroundColour(self.color_y)
                    else:
                        button.SetForegroundColour(self.color_y)
                        button.SetBackgroundColour(self.color_p)
                        
                else:
                    button.SetBackgroundColour(wx.NullColour)
                    button.SetForegroundColour(self.default_fore_color)

                # Store grouped classes to each button.
                button.grouped_classes = sch["courses"]
                    
                # Bind mouse events to the button.
                button.Bind(wx.EVT_LEFT_DOWN, self.on_mouse_down)
                button.Bind(wx.EVT_MOTION, self.on_mouse_move)
                button.Bind(wx.EVT_LEFT_UP, self.on_mouse_up)
                self.btn_schedule_lst[i][j] = button
        self.Bind(wx.EVT_MOTION, self.on_mouse_move)
        self.Bind(wx.EVT_LEFT_UP, self.on_mouse_up)
    
    def get_button_column(self, day):
        """
        This method is used to fetch all buttons pertaining to a certain day
        for the view single day schedule option.
        
        Parameters
        ----------
        day - This is the day for which the user wishes to fetch all clickable
              schedule items.


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/28/15
        Modified by: Justin Brumley 4/7/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        ret = []
        for i in range(len(self.btn_schedule_lst[day - 1])):
            ret.append(self.btn_schedule_lst[day - 1][i])
        
        return ret
    
    def init_ui(self, dim):
        """
        This method handles the setup of the schedule interface.
        
        Parameters
        ----------
        dim - a tuple that holds dimension information for the table in which
              the schedule will reside


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/26/2015
        Modified by: Justin Brumley 4/7/2015 
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Define colors and sizes of buttons and backgrounds.
        self.color_y = (219, 159, 17)
        self.color_p = (70, 22, 107)
        self.color_g = (95, 96, 98)
        self.multi_button_size = (200, 32)
        self.single_button_size = (200, 100)
        self.default_back_color = self.GetTopLevelParent()\
            .GetBackgroundColour()
        self.default_fore_color = self.GetTopLevelParent()\
            .GetForegroundColour()
        self.dim = dim
        self.selected_day = 1
        self.single_day_mode = False
        self.btn_schedule_lst = []
        self.outer_sizer = wx.BoxSizer(wx.VERTICAL)
        self.day_label_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.days_sizer = wx.BoxSizer(wx.VERTICAL)
        self.schedule = self.GetTopLevelParent().sched
        self.str_days = ["Day %s" %(s + 1) for s in range(len(self.schedule))]
        self.str_times = ["Time %s" %(s + 1) for s in range(self.dim[0])]
        
        # We view the multiple day schedule by default.
        self.view_multiple_day_schedule()

    def on_days_clicked(self, event):
        """
        This method handles switching between different days in the single day
        view of the schedule gui.
        
        Parameters
        ----------
        event - required for all wxPython callback functions


        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 3/31/15
        Modified by: Justin Brumley 4/7/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        name = event.GetEventObject().GetLabel()
        lst = name.split()
        num = int(lst[1])
        
        if self.selected_day != num:
            self.selected_day = num
            self.setup_day_labels()
            self.setup_days(self.selected_day)

    def on_mouse_down(self, event):
        """
        This method works in conjunction with the on_mouse_up and the
        on_mouse_move callbacks to simulate drag and drop functionality.
        
        Parameters
        ----------
        event - parameter that is required for all wxPython event callbacks

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/9/15
        Modified by: Jonathon Marlar 4/18/2015
        Files accessed:
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
    
        # Get the calling object.
        obj = event.GetEventObject()
        
        if obj.GetLabel() != "" and type(obj) is wx.Button:
            posx, posy = self.ScreenToClient(obj.GetPositionTuple())
            dx, dy = self.ScreenToClient(wx.GetMousePosition())
            obj._x, obj._y = (posx-dx, posy-dy)
            
            self.btn_positions["x"] = obj
            self.btn_positions["pos"] = (obj._x, obj._y)
            obj.Raise()

            # One second wait time for clicking.
            self.now = time.time()
            self.future = self.now + 0.3
    
    def on_mouse_move(self, event):
        """
        This method works in conjunction with the on_mouse_down and the
        on_mouse_up callbacks to simulate drag and drop functionality.
        
        Parameters
        ----------
        event - parameter that is required for all wxPython event callbacks

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/9/15
        Modified by: 
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        # If an object is present in the dictionary, set the object's position
        # to the mouse cursor.
        if "x" in self.btn_positions and not self.single_day_mode:
            obj = self.btn_positions["x"]
            x, y = wx.GetMousePosition()
            obj.SetPosition(wx.Point(x+obj._x, y+obj._y))
        
        if "x" in self.btn_positions and self.single_day_mode:
            del self.btn_positions["x"]
            del self.btn_positions["pos"]
  
    def on_mouse_up(self, event):
        """
        This method works in conjunction with the on_mouse_down and the
        on_mouse_move callbacks to simulate drag and drop functionality.
        
        Parameters
        ----------
        event - parameter that is required for all wxPython event callbacks

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/9/15
        Modified by: Jonathon Marlar 4/18/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
    
        sz = None
        if not self.single_day_mode:
            # Setup sizer, flags, and styles of buttons for single day view.
            sz = self.time_grid
            flg = wx.ALIGN_TOP|wx.ALIGN_RIGHT|wx.BOTTOM 
            bdr = 13
        
        if "x" in self.btn_positions:
            obj = self.btn_positions["x"]
            
            obj_above = None
            obj_below = None
            
            # Hide the object temporarily so that when the mouse is released,
            # the dragged object isn"t detected.
            obj.Hide()
            m_position = wx.GetMousePosition()
            # Get positions for 10 pixels above and 10 pixels below where the
            # mouse is released to detect above and below.
            m_up = wx.Point(m_position[0], m_position[1]-10)
            m_down = wx.Point(m_position[0], m_position[1]+10)
            obj_above = wx.FindWindowAtPoint(m_up)
            obj_below = wx.FindWindowAtPoint(m_down)
            
            # If the schedule button is not dragged between two schedules.
            if obj_above != None and obj != obj_above and \
            obj_above.GetLabel() != "scrolledpanel" and \
            type(obj_above) is wx.Button and \
            type(obj_below) is wx.Button and obj_above == obj_below:
                temp_text = obj.GetLabel()
                obj.SetLabel(obj_above.GetLabel())
                obj_above.SetLabel(temp_text)
        
                self.recolor(obj_above)
                self.recolor(obj)

                self.schedule[obj_above.col][obj_above.row], \
                    self.schedule[obj.col][obj.row] = \
                    self.schedule[obj.col][obj.row], \
                    self.schedule[obj_above.col][obj_above.row]

                # Swap button's associated grouped classes.
                if obj_above.GetLabel() == "" or obj_above.GetLabel() == " ":
                    obj_above.grouped_classes = obj.grouped_classes
                    obj.grouped_classes = "None"
                else:
                    obj.grouped_classes, obj_above.grouped_classes = \
                        obj_above.grouped_classes, obj.grouped_classes
                # Then, swap the button's start times, end times.
                self.schedule[obj_above.col][obj_above.row]["exam_start_time"], \
                    self.schedule[obj.col][obj.row]["exam_start_time"] = \
                    self.schedule[obj.col][obj.row]["exam_start_time"], \
                    self.schedule[obj_above.col][obj_above.row]["exam_start_time"]
                self.GetTopLevelParent().has_been_modified = True
            
            # If the schedule button is dragged between two schedule buttons.
            elif obj_above != obj_below and \
            (type(obj_above) is wx.Button and type(obj_below) is wx.Button or\
             type(obj_above) is wx.StaticText and type(obj_below) is \
             wx.Button):
                if obj_below.GetLabel() != "" and obj_below.GetLabel() != " ":
                    self.shift_down(obj_above, obj)
                    self.GetTopLevelParent().has_been_modified = True
            
            # Otherwise, return the schedule button to its original position.
            else:
                obj.Show()
                obj.SetPosition(self.btn_positions["pos"])
                if time.time() < self.future:
                    self.show_grouped_classes(obj)
            
            obj.Show()
            
            del self.btn_positions["x"]
            del self.btn_positions["pos"]
            self.Layout()
            
    def recolor(self, btn):
        """
        This method is used to set buttons to their proper colors depending
        on their location in the grid and their labels.
        
        Parameters
        ----------
        btn - the button to be recolored
                        
        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/16/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        if not self.single_day_mode:
            sz = self.sizer
            # Color the label appropriately based on position and label.
            if btn.GetLabel() != "":
                if sz.GetItemPosition(btn).GetRow() % 2 == 0:
                    btn.SetBackgroundColour(self.color_p)
                    btn.SetForegroundColour(self.color_y)
                else:
                    btn.SetBackgroundColour(self.color_y)
                    btn.SetForegroundColour(self.color_p)
            
            else:
                btn.SetBackgroundColour(wx.NullColour)
                btn.SetForegroundColour(wx.NullColour)
        else:
            if btn.GetLabel() != "":
                if sz.GetItemPosition(btn).GetRow() % 2 != 0:
                    btn.SetBackgroundColour(self.color_p)
                    btn.SetForegroundColour(self.color_y)
                else:
                    btn.SetBackgroundColour(self.color_y)
                    btn.SetForegroundColour(self.color_p)
            
            else:
                btn.SetBackgroundColour(wx.NullColour)
                btn.SetForegroundColour(wx.NullColour)
    
    def setup_days(self, day):
        """
        This method sets up the day buttons for the single day view mode.
        
        Parameters
        ----------
        day - an integer that specifies the day for which to display the class
              schedule


        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/31/15
        Modified by: Justin Brumley 4/7/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Hide all buttons before getting the appropriate column.
        for btn in self.btn_single_days:
            btn.Show(False)
            
        # Clear the single_day_sizer, delete its children, and reinitialize.
        self.single_day_sizer.Clear(False)
        self.days_sizer = wx.GridBagSizer(0, 0)
        self.single_day_sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        # Get the appropriate day information.
        self.btn_single_days = self.get_button_column(day)
        
        # Make all appropriate buttons visible and add them to the days_sizer.
        for i, btn in enumerate(self.btn_single_days):
            btn.Show(True)
            self.days_sizer.Add(btn, pos=(i, 0), flag=wx.ALL, border=1)
        
        # Set up the time labels for single day mode, setup sizers, and 
        # rearrange screen items to appropriate places.   
        self.add_single_day_time_labels()
        self.single_day_sizer.Add(self.days_sizer, flag=wx.LEFT|wx.RIGHT,
            border=60)
        self.outer_sizer.Add(self.single_day_sizer, flag=wx.ALIGN_LEFT|wx.ALL, 
            border=20)
        self.single_day_sizer.Layout()
        self.outer_sizer.Layout() 
        self.Layout()   

    def setup_day_labels(self):
        """
        This method handles the actual alteration of the Day labels in the
        single day view mode when the user clicks a different day.
        
        Parameters
        ----------
        None

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/7/15
        Modified by: Justin Brumley 4/30/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Clear the old label sizer out before redrawing.
        self.day_label_sizer.Clear(True)
        
        # Loop through the days string and check for the current day to handle
        # formatting.
        for st in self.str_days:
            btn = wx.StaticText(self, label=st)
            lst = st.split()
            if int(lst[1]) == self.selected_day:
                btn.SetForegroundColour((0, 0, 0))
                font = btn.GetFont()
                font.SetWeight(wx.BOLD)
                btn.SetFont(font)
            else:
                btn.SetForegroundColour(self.color_g)
                font = btn.GetFont()
                font.SetWeight(wx.NORMAL)
                btn.SetFont(font)
            
            # Bind function to the callback self.on_days_clicked.
            btn.Bind(wx.EVT_LEFT_DOWN, self.on_days_clicked)
        
            self.day_label_sizer.Add(btn,
                flag=wx.ALIGN_TOP|wx.LEFT|wx.RIGHT|wx.BOTTOM, border=30)
            
        self.day_label_sizer.ShowItems(True)

    def setup_multiple_day_time_labels(self):
        """
        This method sets up the time labels for the multiple day view of
        the schedule.
        
        Parameters
        ----------
        None

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/7/15
        Modified by: Justin Brumley 4/30/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
           
        temp_str = []    
        exam_start_list = self.GetTopLevelParent().bindings["get_start_times"]() 
        # Inserting the items into the time grid
        for j, exams in enumerate(exam_start_list[0]):
            temp_str.append(wx.StaticText(self, 
                label=exams.strftime("%I:%M %p")))
                
        for i, lbl in enumerate(temp_str):
            self.time_grid.Add(lbl,
                flag=wx.ALIGN_TOP|wx.ALIGN_RIGHT|wx.BOTTOM, pos=(i, 0), 
                border=4)
            self.GetTopLevelParent().Layout()
        
    def setup_single_day_time_labels(self):
        """
        This method sets up the time labels for the single day view of
        the schedule.
        
        Parameters
        ----------
        None

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/7/15
        Modified by: Justin Brumley 4/9/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Sets up StaticText objects for all the labels before 
        # add_single_day_time_labels is called.
        exam_start_list = self.GetTopLevelParent().bindings["get_start_times"]()
        for j, sch in enumerate(exam_start_list[0]):
            self.lbl_single_day_times.append(wx.StaticText(self, 
                label=sch.strftime("%I:%M %p")))

    def shift_down(self, btn_start_object, btn_replace):
        """
        This method works in conjunction with the on_mouse_down and the
        on_mouse_move callbacks to simulate drag and drop functionality.
        
        Parameters
        ----------
        btn_start_object - the starting button from where the buttons need
                           to be shifted down
                        
        btn_replace - the button to move into the newly created space

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 4/15/15
        Modified by: Justin Brumley 4/16/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        x, y = self.sizer.GetItemPosition(btn_start_object)
        child = btn_start_object

        # Traverse to the bottom of the column.
        while child != None:
            temp_pos = (x,y)
            temp_child = child

            child = self.sizer.FindItemAtPosition(temp_pos)
            x += 1

        # If the button is not blank no more exams can be scheduled for that day.
        last_btn = temp_child.GetWindow()
        if last_btn.GetLabel() != " " and last_btn.GetLabel() != "":
            wx.MessageBox("Too many exams for this day", "Error",
                wx.OK)
        else:
            x,y = self.sizer.GetItemPosition(last_btn)
            orig_x = self.sizer.GetItemPosition(btn_start_object).GetRow() + 1
            while x != orig_x:
                second_last_btn = self.sizer.FindItemAtPosition((x-1,y)).GetWindow()

                # Swap labels.
                temp_text = last_btn.GetLabel()
                last_btn.SetLabel(second_last_btn.GetLabel())
                second_last_btn.SetLabel(temp_text)
                self.recolor(last_btn)

                # Swap grouped classes.
                last_btn.grouped_classes, second_last_btn.grouped_classes = \
                    second_last_btn.grouped_classes, last_btn.grouped_classes
                
                # Get the next button.
                x -= 1
                temp_child = self.sizer.FindItemAtPosition((x,y))
                last_btn = temp_child.GetWindow()

            # Now swap the data for the original button and the dragged one.
            btn_swap = self.sizer.FindItemAtPosition((orig_x,y)).GetWindow()
            btn_swap.SetLabel(btn_replace.GetLabel())
            btn_replace.SetLabel("")

            btn_swap.grouped_classes, btn_replace.grouped_classes = \
                btn_replace.grouped_classes, btn_swap.grouped_classes

            self.recolor(btn_replace)
            self.shift_schedule_down(btn_start_object, btn_replace)

    def shift_schedule_down(self, btn_start_object, btn_replace):
        """
        This method works in conjunction with the shift down method and it
        updates the schedule information contained in the MainWindow class.
        
        Parameters
        ----------
        btn_start_object - the starting button from where the buttons need
                           to be shifted down
                        
        btn_replace - the button to move into the newly created space

        Return
        ------
        None

        Author(s): Justin Brumley
        Created: 5/4/15
        Modified by:
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        temp_child = btn_start_object
        empty_sched = {
                        "days": "",
                        "exam_start_time": "",
                        "class_time": "",
                        "courses": [],
                        "enrollment": 0
                    }
        
        begin_row_pos = temp_child.row + 1
        begin_col_pos = temp_child.col
        
        # Add an empty schedule the end of the day.
        self.schedule[begin_col_pos].append(empty_sched)
    
        end_row_pos = len(self.schedule[begin_col_pos]) - 1       

        # Move the empty schedule up to the new position.
        while end_row_pos > begin_row_pos:
            # Swap schedule places.
            self.schedule[begin_col_pos][end_row_pos], \
                self.schedule[begin_col_pos][end_row_pos-1] = \
                self.schedule[begin_col_pos][end_row_pos-1], \
                self.schedule[begin_col_pos][end_row_pos]

            # Swap the buttons' start times.
            self.schedule[begin_col_pos][end_row_pos]["exam_start_time"], \
                self.schedule[begin_col_pos][end_row_pos-1]["exam_start_time"] = \
                self.schedule[begin_col_pos][end_row_pos-1]["exam_start_time"], \
                self.schedule[begin_col_pos][end_row_pos]["exam_start_time"]
                
            end_row_pos -= 1
        
        # Swap the original button's row and column position with the empty
        # schedule's position, and then set the original button's schedule
        # to be empty.
        self.schedule[begin_col_pos][end_row_pos] = \
            self.schedule[btn_replace.col][btn_replace.row]
        self.schedule[btn_replace.col][btn_replace.row] = empty_sched

    def show_grouped_classes(self, btn):
        """
        This method displays the grouped classes in a modal window whenever the
        user clicks on a class in any schedule view.
        
        Parameters
        ----------
        btn - Clicked by user to show groupings.

        Return
        ------
        None


        Author(s): Jonathon Marlar
        Created: 4/18/15
        Modified by: Jonathon Marlar 4/18/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        classes = ""
        for each_class in btn.grouped_classes:
            classes = classes + each_class["days"] + " " + \
                each_class["start_time"].strftime("%I:%M %p") + " - " + \
                each_class["end_time"].strftime("%I:%M %p") + "\n"
        wx.MessageBox(str(classes), "Classes In This Group",
            wx.OK|wx.ICON_INFORMATION)

    def view_multiple_day_schedule(self):
        """
        This method displays the schedule in the multiple day format.
        
        Parameters
        ----------
        None

        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/28/15
        Modified by: Justin Brumley 4/19/2015
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Destroy all the objects in the old schedule, so we don't have multiple
        # schedules piling up.
        for child in self.GetChildren():
            child.Destroy()
            
        self.multiple_day_time_labels = []
        
        # Initializing self.btn_schedule_lst.
        self.btn_schedule_lst = []
        for i, day in enumerate(self.schedule):
            self.btn_schedule_lst.append([])
            for exam in self.schedule[i]:
                self.btn_schedule_lst[i].append(None)
        
        outer_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer = wx.GridBagSizer()
        
        # Setting a sizer to handle the times.    
        self.time_grid = wx.GridBagSizer(wx.VERTICAL)
        
        self.setup_multiple_day_time_labels()
        
        # Preparing the day labels.
        for i in range(len(self.schedule)):
            self.sizer.Add(wx.StaticText(self, 
                label="Day %d" %(i + 1), style=wx.ALIGN_CENTER), 
                flag=wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.ALIGN_CENTER|wx.EXPAND, 
                pos=(0, i), border=13)
        
        # Create the multiple day view schedule.  
        self.build_schedule(False)
        
        # Then add the multiple day view schedule to the sizer.
        for i in range(len(self.btn_schedule_lst)):
            for j in range(len(self.btn_schedule_lst[i])):
                self.sizer.Add(self.btn_schedule_lst[i][j], pos=(j + 1, i),
                    flag=wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER|wx.EXPAND,
                    border=20)
        
            
        outer_sizer.Add(self.time_grid, flag=wx.TOP, border=50)
        outer_sizer.Add(self.sizer, flag=wx.ALL, border=20)
            
        self.SetAutoLayout(1)
        self.SetSizerAndFit(outer_sizer)
        self.SetupScrolling()
        self.sizer.Layout()
        self.Layout()
        self.single_day_mode = False
    
    def view_single_day_schedule(self):
        """
        This method displays the schedule in single day format.
        
        Parameters
        ----------
        None

        Return
        ------
        None


        Author(s): Justin Brumley
        Created: 3/28/15
        Modified by: Justin Brumley 4/7/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        # Get rid of old schedule so multiple schedules are not drawn.
        for child in self.GetChildren():
            child.Destroy()
            
        self.btn_single_days = []
        self.lbl_single_day_times = []
        self.btn_schedule_lst = []
        
        # Initializing self.btn_schedule_lst.
        self.btn_schedule_lst = []
        for i, day in enumerate(self.schedule):
            self.btn_schedule_lst.append([])
            for exam in self.schedule[i]:
                self.btn_schedule_lst[i].append(None)
        
        #Getting the button grid ready for single day view.
        self.build_schedule(True)
        
        self.time_label_sizer = wx.BoxSizer(wx.VERTICAL)
        self.day_label_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.outer_sizer = wx.BoxSizer(wx.VERTICAL)
        self.single_day_sizer = wx.BoxSizer(wx.HORIZONTAL)
    
        # Setting up the labels for the single day view.
        self.setup_day_labels()
        self.outer_sizer.Add(self.day_label_sizer, 
            flag=wx.LEFT, border=200)
        
        # Setting up the time labels.
        self.setup_single_day_time_labels()
        self.add_single_day_time_labels()
        
        # Set up the single day buttons.
        self.setup_days(self.selected_day)

        self.single_day_mode = True
        
        self.SetAutoLayout(1)
        self.SetupScrolling()
        self.SetSizer(self.outer_sizer)
        self.outer_sizer.Layout()
        self.Layout()
