import re
import datetime
import random
import pickle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Indenter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch

class Schedule(object):
    """
    The Schedule class handles all data and operations related to
    retrieving, formatting, and scheduling courses.
    """
    
    def __init__(self):
        self.weekdays = ["M","T","W","R","F"]
        self.courses = []
        self.parameters = {
            "exam_day_amount": 0,
            "begin_time": datetime.datetime(1990, 1, 1),
            "exam_time": datetime.timedelta(),
            "intermission_time": datetime.timedelta(),
            "lunch_time": datetime.timedelta()
        }
        self.lunch = {
            "start_time": datetime.datetime(1990, 1, 1),
            "end_time": datetime.datetime(1990, 1, 1)
        }
        self.schedule_begin_threshold = datetime.datetime(1990, 1, 1, 7, 30)
        self.schedule_end_threshold = datetime.datetime(1990, 1, 1, 17, 15)
        self.lunch_threshold = datetime.datetime(1990, 1, 1, 11)
        self.earliest_course = datetime.datetime(1990, 1, 1, 7)
        self.latest_course = datetime.datetime(1990, 1, 1, 23, 59)
        self.max_groups = 0
        self.groups = []
        self.night_classes = []
        self.schedule = []
        self.file_a = ""
        self.file_b = ""
        self.group_types = [
            {
                "days": "MWF",
                "hours": 0,
                "minutes": 50,
                "interval": 10
            },
            {
                "days": "TR",
                "hours": 1,
                "minutes": 15,
                "interval": 15
            }
        ]

    def get_schedule_times(self):
        """
        This function generates an empty schedule that will later be
        filled.

        Parameters
        ----------
        None


        Return
        ------
        Returns a tuple where the first element is a list of times (as
        integers) for the beginning of each exam, and the second element
        is a integer representing the starting time of the lunch period.
        Both elements will be empty if the user parameters have not been
        set.


        Author(s): Kyle Kurzhal
        Created: 4/14/15
        Modified by: Kyle Kurzhal; 4/18/15
        Files accessed: None
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """
        
        #Initialize the variables.
        start_times = []
        lunch = datetime.datetime(1990, 1, 1)
        lunch_flag = False
        current_time = self.parameters["begin_time"]

        #Loop until the current time surpasses the end time.
        while current_time < self.schedule_end_threshold:
            #Add the current time to the list.
            start_times.append(current_time)

            #Increment the current time by the exam time.
            current_time += self.parameters["exam_time"]

            #Set the lunch time if the current time is after 11pm.
            if current_time >= self.lunch_threshold and not lunch_flag:
                lunch_flag = True
                lunch = current_time
                current_time += self.parameters["lunch_time"]
            #Otherwise, increment the current time by the intermission time.
            else:
                current_time += self.parameters["intermission_time"]

        return (start_times, lunch)
                
    def generate_groups(self):
        """
        This function generates the groups of courses, which are either MWF
        or TR and must be between 12:00am-11:59pm.

        
        Parameters
        ----------
        None


        Return
        ------
        The function returns a list of course groups.


        Author(s): Kyle Kurzhal
        Created: 3/23/15
        Modified by: Kyle Kurzhal, 5/1/15
        Files accessed: None
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """
        
        groups = []
        self.max_groups = 0

        #Loop through each type to get all the groups.
        for each_type in self.group_types:

            #Initially set the new start time starting at 7 AM.
            daytime = datetime.datetime(1990, 1, 1, hour = 7, minute = 0)
            combined_minutes = each_type["minutes"] + each_type["interval"]
            class_time_change = datetime.timedelta(hours = each_type["hours"],
                minutes = combined_minutes
            )

            #Get the time delta for the class time, not including the interval.
            end_class_time_change = datetime.timedelta(
                hours = each_type["hours"],
                minutes = each_type["minutes"]
            )
            
            #Loop while until the end of the day is reached.
            while daytime.day == 1:

                #Get the ending time.
                end_time = daytime + end_class_time_change

                #Continue if the end of the class doesn't run into the next day.
                if end_time.day == 1:

                    #Create the new group.
                    new_group = {
                        "days": each_type["days"],
                        "start_time": daytime,
                        "end_time": end_time,
                        "enrollment": 0,
                        "classes": []
                    }

                    #Add the new group to the list of groups.
                    groups.append(new_group)

                    #Add to the maximum possible number of groups if the time
                    #is before the evening classes threshold.
                    if(end_time <= self.schedule_end_threshold):
                        self.max_groups += 1

                    #Reset the times for the next loop.
                    daytime += class_time_change
                else:
                    break

        #Randomize the groups.
        random.shuffle(groups)
        
        return groups

    def generate_schedule(self):
        """
        Generates a schedule for the Schedule instance and

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/18/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        #Get the times for each exam slot and the lunch time.
        times, lunch = self.get_schedule_times()

        #Generate an empty schedule.
        schedule = []
        for each_day in range(0, self.parameters["exam_day_amount"]):
            #Create the a list for the next day.
            schedule.append([])
            #Add the exams slots to each each day.
            for each_time in times:
                schedule[each_day].append(
                    {
                        "days": "",
                        "exam_start_time": each_time,
                        "class_time": "",
                        "courses": [],
                        "enrollment": 0
                    }
                )

        schedule_day = 0

        #Fill the schedule.
        for each_group in self.groups:
            
            #Create the variable representing the index of the first empty
            #slot.
            first_empty_slot = None
            append_flag = False
            
            #Loop through each exam in the current day of the schedule.
            for each_exam in schedule[schedule_day]:

                #If the slot is empty, then mark it and check if a group
                #can be appended.
                if len(each_exam["days"]) == 0:

                    #Get the index of the current slot if the first empty
                    #slot has not yet been found.
                    if first_empty_slot is None:
                        first_empty_slot = schedule[schedule_day].index(each_exam)

                    #Raise the flag to fill the slot.  If the start time of
                    #the group is at or after the exam time, then use that
                    #slot.
                    if (each_group["start_time"]
                        >= each_exam["exam_start_time"]
                    ):
                        first_empty_slot = schedule[schedule_day].index(each_exam)
                        append_flag = True

                #If the the start time for the group is before the earliest
                #exam time and if an empty slot has been found, then fill
                #the exam slot.
                if (each_group["start_time"] < times[0]
                    and first_empty_slot is not None
                ):
                    append_flag = True

                #If the end of the slots has been reached and
                #an empty slot has been found, then fill the first empty
                #slot.
                if schedule[schedule_day][-1] == each_exam:
                    if first_empty_slot is not None:
                        append_flag = True

                    #Otherwise, if no empty slots have been found, then
                    #raise an exception, because this should not happen.
                    else:
                        raise Exception("""Schedule is not filling
                            appropriately. No exam slots left for day """
                            + str(schedule_day + 1)
                        )

                #If a group can be appended, then do so and move on to the
                #next group.
                if append_flag == True:
                    schedule[schedule_day][first_empty_slot]["days"] = (
                        each_group["days"]
                    )
                    schedule[schedule_day][first_empty_slot]["courses"] = (
                        each_group["classes"]
                    )
                    schedule[schedule_day][first_empty_slot]["class_time"] = (
                        each_group["start_time"].strftime("%I:%M %p") + ' - '
                        + each_group["end_time"].strftime("%I:%M %p")
                    )
                    schedule[schedule_day][first_empty_slot]["enrollment"] = (
                        each_group["enrollment"]
                    )
                    first_empty_slot = None
                    append_flag = False
                    break
                    
            #Go to the next day.
            schedule_day += 1
            schedule_day = schedule_day % self.parameters["exam_day_amount"]
            
        #Randomly mix the days.
        random.shuffle(schedule)

        self.schedule = schedule

        #Get the exam ending times for the night classes.
        for each_class in self.night_classes:
            each_class["exam_end_time"] = (each_class["start_time"]
                + self.parameters["exam_time"]
            )

    def group_courses(self):
        """
        This function groups the courses by start time.  Each course is
        grouped with other courses on the same day at the same time.
        Night classes are grouped into their own list.

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Amberly Smith, Morgan Burcham
        Created: 3/23/15
        Modified by: Kyle Kurzhal; 5/4/15
        Files accessed: None 
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        #Store the list of potential classtimes.
        self.groups = self.generate_groups()
        
        #Create an empty list to contain any night classes.
        self.night_classes = []

        #Iterate through the courses.
        for course in self.courses:

            #Only add a course if there are 1 of more students.
            if course["enrollment"] > 0:
            
                #Check for a night class.  If so, add the course to the list
                #of night classes.
                if course["start_time"] >= self.schedule_end_threshold:
                    course["exam_end_time"] = datetime.datetime(1990, 1, 1)
                    self.night_classes.append(course)

                else:
                    #Iterate through the potential class times.
                    for each_group in self.groups:

                        #Create an empty intermission time delta needed for
                        #the range check.
                        intermission_delta = datetime.timedelta()

                        #Get the intermission time delta needed for the range
                        #check below.
                        for each_type in self.group_types:
                            if each_group["days"] == each_type["days"]:
                                intermission_delta = datetime.timedelta(
                                    minutes = each_type["interval"]
                                )
                                break
                        
                    
                        #See if the course is in a valid time range.  The
                        #intermission delta is added to account for any
                        #classes that start between the intevals of the groups.
                        if (course["start_time"] <
                            (each_group["end_time"] + intermission_delta)
                            and
                            course["start_time"] >= each_group["start_time"]
                        ):
                            #Check if any weekdays are found in the course that
                            #match days in the current group.
                            if any(
                                [letter in each_group["days"]
                                    for letter in course["days"]
                                ]
                            ):
                                each_group["enrollment"] += course["enrollment"]
                                each_group["classes"].append(course)
                                break

        #Remove all the groups with an enrollment of 0.
        self.groups = [each_group
            for each_group in self.groups
            if each_group["enrollment"] != 0
        ]

        #Sort the groups by enrollment.
        self.sort_groups_by_enrollment()

    def load_courses_from_file(self, file_name):
        """
        This function loads all courses from the specified files.

        Parameters
        ----------
        file_name - The name of the file, including the path, that
        contains courses that must be loaded.


        Return
        ------
        If a known error is encountered, then the function throws an
        exception.


        Author(s): Amberly Smith, Kyle Kurzhal
        Created: 3/16/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: Depends on the file passed to the function.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        #Open the file.
        with open(file_name, "r") as file_handle:
            #Start the line counter.
            line_count = 0
            
            #Throw out the first line.
            file_handle.readline()

            #Get the first line of actual data and then loop while there are
            #still lines.
            each_line = file_handle.readline()
            while each_line:
                #Incease the line count by 1.
                line_count = line_count + 1
                
                #Start/Restart with the empty course data.
                course_data = {
                    "days": "",
                    "start_time": datetime.datetime(1990, 1, 1),
                    "end_time": datetime.datetime(1990, 1, 1),
                    "enrollment": 0,
                }

                #Start/Restart the weekday_flag as True.
                weekday_flag = True

                #Grab the course days.
                while weekday_flag:
                    #Set the flag to false for the case that no match is found.
                    weekday_flag = False
                    for each_letter in self.weekdays:
                        if each_letter == each_line[0]:
                            weekday_flag = True
                            course_data["days"] += each_letter
                            each_line = each_line[1:]
                            break

                #Split the rest of the line in order to get the course times.
                #Any exceptions will be caught.
                try:
                    number_list = each_line.split(",")

                    #Get the enrollment number.
                    course_data["enrollment"] = int(number_list[1])

                    #Grab the military times and remove all spaces.
                    number_list = number_list[0].replace(' ', '')

                    #Split the starting and ending times into separate list
                    #items.
                    number_list = number_list.split("-")

                    #Convert the string values to the matching integers and
                    #store in the course data found for this loop iteration.
                    for index, key in [(0, "start_time"), (1, "end_time")]:
                        hour = int(number_list[index][:2])
                        minute = int(number_list[index][2:])
                        course_data[key] = datetime.datetime(1990, 1, 1,
                            hour, minute
                        )

                    #Validate the input retrieved from the current line.
                    self.validate_course(course_data)

                    #Put the course in the list of courses
                    self.courses.append(course_data)

                #Catch incorrect formats that prevent loading the data.
                except ValueError as error:
                    self.courses = []
                    raise Exception("Error--Incorrect format - line " + str(line_count))

                #Catch all other errors that may pop up.
                except Exception as error:
                    self.courses = []
                    raise Exception(str(error) + " - line " + str(line_count))
                
                #Get the next line
                each_line = file_handle.readline()

        #Store the file name associated with the courses.
        self.file_b = file_name

    def load_parameters_from_file(self, file_name):
        """
        This function loads the exam parameters from a csv file.

        Parameters
        ----------
        file_name - The name of the file, including the path, that
        contains courses that must be loaded.


        Return
        ------
        If a known error is encountered, then the function throws an
        exception.


        Author(s): Kyle Kurzhal
        Created: 3/22/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: The file passed to the function.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        with open(file_name, "r") as file_handle:
            file_content = file_handle.read()
            file_parameters = file_content.split(",")
            begin_time = file_parameters[1].replace(" ", "")

            try:
                temp_parameters = {
                    "exam_day_amount": int(file_parameters[0]),
                    "begin_time": datetime.datetime(1990, 1, 1,
                        int(begin_time[:2]),
                        int(begin_time[2:])
                    ),
                    "exam_time": datetime.timedelta(
                        minutes = int(file_parameters[2])
                    ),
                    "intermission_time": datetime.timedelta(
                        minutes = int(file_parameters[3])
                    ),
                    "lunch_time": datetime.timedelta(
                        minutes = int(file_parameters[4])
                    )
                }


                self.validate_parameters(temp_parameters)
            except Exception as error:
                raise error

        self.parameters = temp_parameters

        #Store the file name associated with the parameters.
        self.file_a = file_name

    @staticmethod
    def load_schedule_from_umes(file_name):
        """
        A class method that accesses a file containing data from a
        previous schedule instance and essentially "opens" that
        instance.

        Parameters
        ----------
        file_name - The name of the file that will be opened.


        Return
        ------
        Returns an instance of the class filled with data from a
        previous schedule.


        Author(s): Kyle Kurzhal
        Created: 4/5/15
        Modified by: Kyle Kurzhal, 4/5/15
        Files accessed: The file name passed in.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        schedule = None
        #Load the schedule instance from the file.
        with open(file_name, "r") as file_handle:
            schedule = pickle.load(file_handle)

        return schedule


    def save_parameters_to_file(self, file_name):
        """
        This method saves the list of parameters for the schedule to a
        CSV file.

        Parameters
        ----------
        file_name - The name of the file that the parameters will be
                    saved to.


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/5/15
        Modified by: Kyle Kurzhal, 4/18/15
        Files accessed: The file specified when passed.
        Date tested: 4/29/15
        Approved by: Morgan Burcham
        """
        #Get all the parameters in the proper order.
        output_list = []
        output_list.append(str(self.parameters["exam_day_amount"]))
        output_list.append(str(self.parameters["begin_time"].strftime("%H%M")))
        output_list.append(str(self.parameters["exam_time"].seconds / 60))
        output_list.append(str(self.parameters["intermission_time"].seconds / 60))
        output_list.append(str(self.parameters["lunch_time"].seconds / 60))
        
        #Join the list of parameters into a string separated by commas.
        output_string = ",".join(output_list)

        #Write the parameters to a file.
        with open(file_name, "w") as file_handle:
            file_handle.write(output_string)

        #Store the file name associated with the parameters.
        self.file_a = file_name

    def save_schedule_to_csv(self, file_name):
        """
        This method saves the schedule as a CSV file.

        Parameters
        ----------
        file_name - The name of the file that the schedule will be saved
                    to.


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/23/15
        Modified by: Kyle Kurzhal; 5/2/15
        Files accessed: The file name passed in.
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """
        
        #Set the list of strings that will be written to the CSV file.
        output_list = []
        
        #Write the header for the CSV file.
        output_list.append("Classes, Class Start Time, Class End Time,"
            + "Exam Day, Exam Start Time, Exam End Time"
        )

        #Loop through each day.
        for index, each_day in enumerate(self.schedule):
            #Loop through each exam in the current day.
            for each_exam in each_day:
                
                #Get the ending exam time as a string.
                end_exam_time = (each_exam["exam_start_time"]
                    + self.parameters["exam_time"]
                )

                exam_list = []

                #Get the groups of courses that must be displayed.
                for each_class in each_exam["courses"]:
                    each_line = []

                    #Get the class days and times.
                    each_line.append(each_class["days"])
                    each_line.append(each_class["start_time"].strftime("%I:%M %p"))
                    each_line.append(each_class["end_time"].strftime("%I:%M %p"))

                    #Get the exam day and times.
                    each_line.append("Day " + str(index + 1))
                    each_line.append(
                        each_exam["exam_start_time"].strftime("%I:%M %p")
                    )
                    each_line.append(end_exam_time.strftime("%I:%M %p"))

                    #Add the class/exam data to the exam list, separated by
                    #commas.
                    exam_list.append(", ".join(each_line))

                #Sort the exam list.
                exam_list.sort()

                #Add the exam list to the final output list.
                output_list += exam_list

        exam_list = []

        #Get the night classes.
        for each_class in self.night_classes:
            each_line = []

            #Get the class days and times.
            each_line.append(each_class["days"])
            each_line.append(each_class["start_time"].strftime("%I:%M %p"))
            each_line.append(each_class["end_time"].strftime("%I:%M %p"))

            #Get the exam day and times.
            each_line.append("Night")
            each_line.append(each_class["start_time"].strftime("%I:%M %p"))
            each_line.append(each_class["exam_end_time"].strftime("%I:%M %p"))

            #Add the class/exam data to the exam list, separated by
            #tabs.
            exam_list.append(", ".join(each_line))

        #Sort the exam list.
        exam_list.sort()

        #Add the night classes list to the final output list.
        output_list += exam_list

        #Write all the data to the file.
        with open(file_name, "w") as file_handle:
            file_handle.write("\n".join(output_list))

    def save_schedule_to_pdf(self, file_name):
        """
        This method saves the schedule as a PDF file.

        Parameters
        ----------
        file_name - The name of the file that the schedule will be saved
                    to.


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/22/15
        Modified by: Kyle Kurzhal; 4/23/15
        Files accessed: The file name passed in.
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """
        
        #Set up the document template.
        doc = SimpleDocTemplate(file_name)
        normal_style = ParagraphStyle("normal", fontSize = 12)
        time_indent_style = ParagraphStyle("indent", leftIndent = .5 * inch,
            fontSize = 12
        )
        exam_indent_style = ParagraphStyle("indent", leftIndent = inch,
            fontSize = 12
        )

        #Begin the content with a space.
        #content = [Spacer(inch, inch)]
        content = []

        #Loop through each day.
        for index, each_day in enumerate(self.schedule):

            #Get the string for the current day and append it to the PDF body
            #as a header for the section.
            day_line = "<u>Day " + str(index + 1) + "</u>"
            paragraph_line = Paragraph(day_line, normal_style)
            content.append(paragraph_line)
            content.append(Spacer(0, .2 * inch))
            
            #Loop through each exam in the current day.
            for each_exam in each_day:
                if len(each_exam["courses"]):
                    #Get the exam time as a string.
                    end_exam_time = (each_exam["exam_start_time"]
                        + self.parameters["exam_time"]
                    )
                    
                    exam_time_str = (
                        "<b>Exam Time: "
                        + each_exam["exam_start_time"].strftime("%I:%M %p")
                        + " - " + end_exam_time.strftime("%I:%M %p")+ "</b>"
                    )

                    group_str = (
                        "<b>Group: " + each_exam["days"] + " "
                        + each_exam["class_time"] + "</b>"
                    )
                    
                    #Get the exam time header.
                    paragraph_line = Paragraph(exam_time_str, time_indent_style)
                    content.append(paragraph_line)
                    content.append(Spacer(0, .1 * inch))

                    #Get the group header.
                    paragraph_line = Paragraph(group_str, time_indent_style)
                    content.append(paragraph_line)
                    content.append(Spacer(0, .2 * inch))

                    exam_list = []

                    #Get the groups of courses that must be displayed.
                    for each_class in each_exam["courses"]:
                        
                        #Get the class time as a string.
                        class_time = (each_class["start_time"].strftime("%I:%M %p")
                            + " - " + each_class["end_time"].strftime("%I:%M %p")
                        )

                        #Get the content for the exam that is to fill a line.
                        exam_line = each_class["days"] + "   " + class_time
                        exam_list.append(exam_line)

                    for exam_line in sorted(exam_list):
                        
                        #Append the line to the content.
                        paragraph_line = Paragraph(exam_line, exam_indent_style)
                        content.append(paragraph_line)
                        content.append(Spacer(1, .1 * inch))

                    content.append(Spacer(inch, .2 * inch))

            #Append a space to the content between the day sections.
            content.append(Spacer(1, .2 * inch))

        #Get the night classes header.
        content.append(Spacer(1, .2 * inch))
        night_line = "<u>Night Classes</u>"
        paragraph_line = Paragraph(night_line, normal_style)
        content.append(paragraph_line)
        content.append(Spacer(0, .2 * inch))

        for each_class in self.night_classes:
            exam_str = ("<b>Exam Time: "
                + each_class["start_time"].strftime("%I:%M %p") + " - "
                + each_class["exam_end_time"].strftime("%I:%M %p")
                + "</b>"
            )

            paragraph_line = Paragraph(exam_str, time_indent_style)
            content.append(paragraph_line)
            content.append(Spacer(0, .1 * inch))
            
            class_str = (
                each_class["days"] + " "
                + each_class["start_time"].strftime("%I:%M %p")
                + " - " + each_class["end_time"].strftime("%I:%M %p")
            )

            paragraph_line = Paragraph(class_str, exam_indent_style)
            content.append(paragraph_line)

            #Only add space if the each_class is not the last night class.
            if each_class != self.night_classes[-1]:
                content.append(Spacer(0, .2 * inch))
        
        #Build/save the PDF.
        doc.build(content)

    def save_schedule_to_text_file(self, file_name):
        """
        This method saves the schedule as a text file.

        Parameters
        ----------
        file_name - The name of the file that the schedule will be saved
                    to.


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/23/15
        Modified by: Kyle Kurzhal; 5/2/15
        Files accessed: The file name passed in.
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """
        #Set the list of strings that will be written to the CSV file.
        output_list = []
        
        #Write the header for the CSV file.
        output_list.append("Classes\tClass Start Time\tClass End Time\t"
            + "Exam Day\tExam Time"
        )
        output_list.append("-------\t----------------\t--------------\t"
            + "--------\t---------------\n"
        )
        
        #Loop through each day.
        for index, each_day in enumerate(self.schedule):
            #Loop through each exam in the current day.
            for each_exam in each_day:
                
                #Get the ending exam time as a string.
                end_exam_time = (each_exam["exam_start_time"]
                    + self.parameters["exam_time"]
                )

                exam_list = []

                #Get the groups of courses that must be displayed.
                for each_class in each_exam["courses"]:
                    each_line = []

                    #Get the class days and times.
                    each_line.append(each_class["days"])
                    each_line.append(each_class["start_time"].strftime("%I:%M %p"))
                    each_line.append(each_class["end_time"].strftime("%I:%M %p"))

                    #Get the exam day and times.
                    each_line.append("Day " + str(index + 1))
                    exam_time = (
                        each_exam["exam_start_time"].strftime("%I:%M %p")
                        + " - "
                        + end_exam_time.strftime("%I:%M %p")
                    )
                    each_line.append(exam_time)

                    #Add the class/exam data to the exam list, separated by
                    #tabs.
                    exam_list.append("\t\t".join(each_line))

                #Sort the exam list.
                exam_list.sort()

                #Add the exam list to the final output list.
                output_list += exam_list

        exam_list = []

        #Get the night classes.
        for each_class in self.night_classes:
            each_line = []

            #Get the class days and times.
            each_line.append(each_class["days"])
            each_line.append(each_class["start_time"].strftime("%I:%M %p"))
            each_line.append(each_class["end_time"].strftime("%I:%M %p"))

            #Get the exam day and times.
            each_line.append("Night")

            exam_time = (
                each_class["start_time"].strftime("%I:%M %p")
                + " - " + each_class["exam_end_time"].strftime("%I:%M %p")
            )

            each_line.append(exam_time)

            #Add the class/exam data to the exam list, separated by
            #tabs.
            exam_list.append("\t\t".join(each_line))

        #Sort the exam list.
        exam_list.sort()

        #Add the night classes list to the final output list.
        output_list += exam_list

        #Write all the data to the file.
        with open(file_name, "w") as file_handle:
            file_handle.write("\n".join(output_list))

    def save_schedule_to_umes(self, file_name):
        """
        This method takes the current instance of the schedule and saves
        it to a file for later loading.

        Parameters
        ----------
        file_name - The name of the file that the schedule will be saved
                    to.


        Return
        ------
        None


        Author(s): Kyle Kurzhal
        Created: 4/5/15
        Modified by: Kyle Kurzhal, 4/5/15
        Files accessed: The file name passed in.
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        #Save the current schedule instance to the file.
        with open(file_name, "w") as file_handle:
            pickle.dump(self, file_handle)

    def set_parameters(self, parameters_dictionary):
        """
        This function is specifically designed to set parameters for the
        exam scheduling.

        Parameters
        ----------
        parameters_dictionary - The dictionary of parameters that will
        be set.


        Return
        ------
        The function raises an exception if the parameters were not
        valid.


        Author(s): Kyle Kurzhal
        Created: 3/22/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        #Validate the parameters.
        try:
            self.validate_parameters(parameters_dictionary)
        except Exception as error:
            raise error

        #Set the parameters.
        self.parameters = parameters_dictionary

    def sort_groups_by_enrollment(self):
        """
        This function checks the enrollment values for the course groupings.
        If no students are enrolled, the class will not be scheduled

        Parameters
        ----------
        None


        Return
        ------
        None


        Author(s): Amberly Smith, Morgan Burcham
        Created: 3/23/15
        Modified by: Kyle Kurzhal, 4/10/15
        Files accessed: None
        Date tested: 5/2/15
        Approved by: Morgan Burcham
        """

        #Sort the course groupings by enrollment in descending order.
        self.groups = sorted(
            self.groups,
            key=lambda enroll: enroll["enrollment"],
            reverse=True
        )

    def validate_course(self, course_dictionary):
        """
        Validates whether or not the weekday, start time, end time, and
        enrollment input are valid.

        Parameters
        ----------
        couse_dictionary - Course dictionary containing the weekdays,
        start times, end times, and enrollments of the exams to be
        scheduled.


        Return
        ------
        Will halt validation and raise an exception if there is invalid
        input and inform user of the invalid input and the type of
        invalid input.


        Author(s): Morgan Burcham
        Created: 3/16/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        #Initially set error message variable to None.
        error_message = ""

        #Set the keys that must be double checked.
        keys = [
            "days",
            "start_time",
            "end_time",
            "enrollment"
        ]

        #Check to make sure that the keys of both dictionaries match.
        if not all([each_key in keys for each_key in course_dictionary.keys()]):
            error_message = ( "Error--dictionary keys do not match.")
        
        #Compare the current weekday value of the exam with a regular
        #expression (MTWRF).
        if not re.search("^M?T?W?R?F?$", course_dictionary["days"]):
            error_message = ("Error--invalid order of days: "
                + course_dictionary["days"]
            )

        #Check if the start time of the current course is before 7:00 am.
        elif (course_dictionary["start_time"] < self.earliest_course
            or course_dictionary["start_time"] > self.latest_course
        ):
            error_message = ("Error--invalid start time for exam: "
                + course_dictionary["start_time"].strftime("%H%M")
            )

        #Check if the end time of the current course is after midnight.
        elif (course_dictionary["end_time"] > self.latest_course
            or course_dictionary["end_time"] < self.earliest_course
        ):
            error_message = ("Error--invalid end time for exam: "
                + course_dictionary["end_time"].strftime("%H%M")
            )

        #Check if the current course has a valid enrollment.
        elif course_dictionary["enrollment"] < 0:
            error_message = ( "Error--invalid enrollment value: "
                + str(course_dictionary["enrollment"])
            )

        if len(error_message) > 0:
            raise Exception(error_message)

    def validate_parameters(self, parameters_dictionary):
        """
        This function validates whether the exam parameters specified
        fall within the correct range of appropriate values.

        Parameters
        ----------
        parameters_dictionary - The dictionary of parameters that will
        be checked.


        Return
        ------
        The function raises an exception if the parameters do not meet
        the specified criteria.


        Author(s): Kyle Kurzhal
        Created: 3/22/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """
        
        error_message = ""
        #update: Setting the minimum times. Maybe set as instance variable?
        min_exam_time = datetime.timedelta(hours = 1, minutes = 15)
        min_intermission_time = datetime.timedelta(minutes = 10)
        max_intermission_time = datetime.timedelta(minutes = 30)
        min_lunch_time = datetime.timedelta(minutes = 0)

        #Check to make sure that the keys of both dictionaries match.
        key_check = map(lambda key: key in self.parameters.keys(),
            parameters_dictionary
        )
        if not all(key_check):
            error_message = ( "Error--dictionary keys do not match.")

        #Check to make sure that the number of exam days are in the correct
        #range.
        elif (parameters_dictionary["exam_day_amount"] > 5
              or parameters_dictionary["exam_day_amount"] <= 0
        ):
            error_message = ( "Error--invalid number of exam days: "
                + str(parameters_dictionary["exam_day_amount"])
            )

        #Check to make sure that the beginning time for exams is in the
        #correct range.
        elif parameters_dictionary["begin_time"] < self.schedule_begin_threshold:
            error_message = ( "Error--invalid beginning time: "
                + parameters_dictionary["begin_time"].strftime("%H%M")
            )

        #Check to make sure that the time for each exam is in the correct
        #range.
        elif parameters_dictionary["exam_time"] < min_exam_time:
            error_message = ( "Error--invalid exam time length: "
                + str(parameters_dictionary["exam_time"].seconds / 60)
            )

        #Check to make sure that the time between exams is in the correct
        #range.
        elif (parameters_dictionary["intermission_time"] < min_intermission_time
            or parameters_dictionary["intermission_time"] > max_intermission_time
        ):
            error_message = ( "Error--invalid intermission time length: "
                + str(parameters_dictionary["intermission_time"].seconds / 60)
            )

        #Check to make sure that the exam days are in the correct range.
        elif parameters_dictionary["lunch_time"] < min_lunch_time:
            error_message = ( "Error--invalid lunch time length: "
                + str(parameters_dictionary["lunch_time"].seconds / 60)
            )
            
        if len(error_message) > 0:
            raise Exception(error_message)

    def validate_schedule(self):
        """
        Validates, in advanced that a schedule can be properly generated
        based on the user parameters and the courses provided.

        Parameters
        ----------
        None


        Return
        ------
        An exception is raised if the schedule is not valid.


        Author(s): Amberly Smith, Kyle Kurzhal, Morgan Burcham
        Created: 4/10/15
        Modified by: Kyle Kurzhal; 5/5/15
        Files accessed: None
        Date tested: 5/5/15
        Approved by: Morgan Burcham
        """

        #Initially set the exam count for the day to 0.
        exam_count = 0

        #Get the difference between the beginning exam and end of schedule
        #threshold times.
        tmp_time_diff = self.schedule_end_threshold - self.parameters["begin_time"]

        #Get the available number of minutes by subtracting the lunch time
        #(in minutes) from the time difference.
        available_time = tmp_time_diff - self.parameters["lunch_time"]

        #Get the number of exams that are possible. Loop until the available
        #time runs out.
        while available_time.days == 0:
            #Subtract the exam time (in minutes) from the available minutes.
            available_time -= self.parameters["exam_time"]

            #Increase the exam count if the available minutes are not over the
            #mark.
            if available_time.seconds >= 0:
                exam_count += 1

            #Subtract the intermission time from the available minutes.
            available_time -= self.parameters["intermission_time"]

        #Get the total number of exams by multiplying the days by exams per day.
        exam_count *= self.parameters["exam_day_amount"]

        if exam_count < self.max_groups:
            error_message = ( """Error--exams exceed user's parameters. Exams
                slots: """ + str(exam_count) + "; Exams expected: "
                + str(self.max_groups)
            )
            raise Exception(error_message)
